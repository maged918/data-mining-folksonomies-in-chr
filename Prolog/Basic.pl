% Author:
% Date: 03/03/2014

:- use_module(library(chr)).

:- chr_constraint   annotation/3, tag/1, link/3, 
 					startClustering/0,
 					currentCluster/1, distance/3, seed/2, tagInCluster/2.

countClusters(2).
linkValue(1).
distanceMeasure('co-occurence').

/*annotation(maged, item1, sports).
annotation(maged, item1, ball).
annotation(maged, item2, movie).
annotation(maged, item2, cinema).
annotation(khaled, item1, sports).
annotation(khaled, item1, football).
annotation(khaled, item2, cinema).
startClustering ==> annotation(_,_,T) | tag(T).*/

annotation(_,_,T) ==> tag(T).
tag(T) \ tag(T) <=> true.

%Building bidirectional co-occurence graph
annotation(_,I,T1), annotation(_, I,T2) ==> T1\=
T2, linkValue(LV) | link(T1,T2,LV).
link(T1,T2,W1), link(T1,T2,W2) <=> W is W1 + W2 | link(T1,T2,W).

startClustering <=> currentCluster(0).

% Random assignment of tags to be initial seeds.
%%NOTE: Very easy using CHR to choose the initial seeds yourself, just specify the constraint instead of using the above rule!
currentCluster(X) <=> countClusters(X) | true.
true \ tag(T), currentCluster(X) <=> X1 is X+1 | seed(T, X), currentCluster(X1).


%Computing distance of remaining tags to seeds. Specify distance measure method using distanceMeasure fact.
seed(Seed, _), tag(Tag), link(Seed,Tag,Weight) ==> distanceMeasure('co-occurence'),
									Distance is 1/Weight | distance(Tag, Seed, Distance).

%Keeping minimum distance only
distance(Tag,_, W1) \ distance(Tag,_,W2) <=> W1 =< W2 | true.

%Assigning tags to clusters when done
distance(Tag, Seed, _), seed(Seed, Cluster) ==> tagInCluster(Tag, Cluster).

%Assigning seeds to clusters as well in the end)

seed(Seed , Cluster) ==> tagInCluster(Seed, Cluster).
