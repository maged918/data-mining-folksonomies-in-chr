:- use_module(library(chr)).

:- chr_constraint start/0, ann/3, item/1, tag/1, tag/2,
							countItems/1, countTags/1, 
							tagVector/1, itemVector/2, sim/3, 
							countClusters/1, countCentroids/1, possibleCentroid/1,
							cluster/4, calculatedSim/1, 
							assign/1, move/1, iterate/1, currentIteration/1.

/* Notes: 
 * Minimum number of clusters is 2. 
 * Iterate(X) represents how many iterations kmeans will run
 * Output is the cluster(Id, Centroid, ItemIds, ItemVectors) constraint.
 * 
 * Query:
 * ann(maged, i1, sports), ann(maged, i1, ball), ann(khaled, i1, sports), ann(khaled, i1, football),
 ann(maged, i2, sports),ann(maged, i2, football),ann(khaled, i2, football),ann(khaled, i2, match),
 ann(maged, i3, cinema),ann(maged, i3, movie),ann(khaled, i3, cinema),ann(khaled, i3, film),
 ann(maged, i4, cinema),ann(maged, i4, film),ann(khaled, i4, film),ann(khaled, i4, comedy), ann(khaled,i4,movie),
 ann(maged, i5, nature),ann(maged, i5, forest),ann(khaled, i5, cinema),ann(khaled, i5, nature),
 ann(maged, i6, trees),ann(maged, i6, forest),ann(khaled, i6, nature),ann(khaled, i6, trees), 
 start, countClusters(3), iterate(3).
 */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROLOG

/*
 * fillZeroes(L, Cnt) ==> Fill list L with #Cnt zeroes.
 */
fillZeroes([0],1).
fillZeroes([0|T], Cnt):- NewCnt is Cnt -1, fillZeroes(T, NewCnt). 

/*
 * increment(L, Idx, NewList) ==> Increment L at position Idx by 1 (L is 1-based)
 */
increment([H|T], 1, [NewH|T]) :- NewH is H+1.
increment([H|T], Idx, [H|NewT]):- Idx > 1, NewIdx is Idx-1, increment(T, NewIdx, NewT). 

/*
 * cosineSim(V1, V2, Sim) ==> Calculates cosine similarity between V1 & V2
 */
cosineSim(V1, V2, Sim):- dotProduct(V1, V2, P), sumSquare(V1, S1), sumSquare(V2, S2),
							sqrt(S1, M1), sqrt(S2, M2), Sim is P/(M1*M2).

/*
 * dotProduct(V1, V2, P) ==> Calculates dot product of vectors V1 & V2
 */
dotProduct([],[],0).
dotProduct([H1|T1], [H2|T2], P):- dotProduct(T1, T2, NewP), Cur is H1 * H2, P is Cur + NewP.
 
/*
 * sumSquare(V, M) ==> Calculates the sum of the squares of elements in vector V.
 */
sumSquare([], 0).
sumSquare([H|T], S):- sumSquare(T,NewS), Cur is H*H, S is Cur + NewS.

/*
 * computeAverage(L, Avg) ==> Returns a list which is the average of the list of lists L.
 * Calls sum, which returns a list containing the sum of all elements at the same index,
 then it calls average, which divides all items in Sum by CntItems 
 */

computeAverage(List, Avg):- List = [H|_], length(H, CntTags), sum(List, 0, CntTags, [], Sum), 
							length(List, CntItems), average(Sum, [], Avg, CntItems). 

/*
 * sum(List, Idx, Len, Temp, Avg) ==> Returns a list (Avg) containing sum of all elements at the same index.
 * Loops from 0 till Len, calling sumAtIndex in every iteration, which returns an element representing the
 sum of all elements at the same index in the list of lists (List). 
 */

sum(_, Len, Len, Sum, Sum).
sum(List, Idx, Len, Temp, Sum):- Idx<Len,sumAtIndex(List, Idx, S), append(Temp, [S], NewTemp),
								NewIdx is Idx+1, sum(List,NewIdx,Len,NewTemp,Sum).

/*
 * sumAtIndex([List|Tail], Idx, Sum):- Returns the sum (Sum) of all elements at the same index (Idx).
 * Keeps calling nth0 on each list in the list of lists until there are no more.
 */
sumAtIndex([], _, 0).
sumAtIndex([List|Tail], Idx, Sum):- nth0(Idx, List, Cur), sumAtIndex(Tail, Idx, Rem), Sum is Cur + Rem.

/*
 * average([H|T], Temp, Avg, CntItems):- Divides all items in [H|T] by CntItems.
 */
average([], Avg, Avg, _).
average([H|T], Temp, Avg, CntItems):- Cur is H/CntItems, append(Temp, [Cur], NewTemp), 
									average(T, NewTemp, Avg, CntItems). 
								 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Generate item vectors step

/*
 * Start counting the number of currentIterations from 1.
 */
iterate(_) ==> currentIteration(1).

/*
 * Generate an empty vector of tags in the beginning
 */
start <=> tagVector([]).

/*
 * Generate all items and keep only one copy of each, and count the number of items.
 */
ann(_, I, _) ==> item(I).
item(I) \ item(I) <=> true.
item(_) ==> countItems(1).
true \ countItems(X), countItems(Y) <=> Sum is X + Y | countItems(Sum).

/*
 * Generate all tags and keep only one copy of each
 */
 ann(_, _, T)  ==> tag(T).
 tag(T) \ tag(T) <=> true.
 
/*
 * Insert each tag in the tag vector, and give it an id which is its position in the vector.
 */
true \ tag(T), tagVector(L) <=> append(L, [T], NewList), length(NewList, Id) | tag(Id, T), tagVector(NewList).

/*
 * Count number of unique tags
 */
tagVector(Vector) ==> length(Vector, Cnt), countTags(Cnt).

/*
 * Generate a zero-filled tag vector for all items in the beginning
 */
item(I), countTags(Cnt) ==> fillZeroes(L, Cnt) | itemVector(I, L).

/*
 * For each annotation, increment the item vector at the position of the tag by 1.
 * Must remove ann as not to keep applying the rule for each new itemVector
 * Could replace ann by another constraint at beginning to preserve
 */
tag(Id, T) \ ann(_, I, T), itemVector(I, L) <=> increment(L, Id, NewList) | itemVector(I, NewList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Generate centroids step

/*
 * Start counting number of initial centroids from 1.
 */
countClusters(_) ==> countCentroids(1).

/*
 * Each item is a possible centroid at the start.
 */
item(I) ==> possibleCentroid(I).

/*
 * Initiate a cluster using one of the possible centroids, and increment count of initial centroids by one.
 * Each cluster is initially empty, and its centroid is the item vector of the possible centroid.
 */
countClusters(Cl), itemVector(I, L) \ possibleCentroid(I), countCentroids(Cn) <=> 
				Cn =< Cl, Cnew is Cn +1 | cluster(Cn, L, [], []), countCentroids(Cnew).

/*
 * Remove remaining possible of centroids once all needed clusters have been initiated.
 */
countClusters(Cl), countCentroids(Cn) \ possibleCentroid(_) <=>
				Cn > Cl | true.

/*
 * Remove count of initial centroids once all clusters have been initiated (From 1 to CountClusters)
 * This prevents this section of rules to be applied more than once.
 */
countClusters(Cl) \ countCentroids(Cn) <=> Cn > Cl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Assign to cluster step

/*
 * Calculate the cosine similarity between all items' item vectors (L1) and clusters' centroids (L2).
 */
itemVector(I, L1), cluster(C, L2, [], []) ==> cosineSim(L1, L2, Sim) | sim(I, C, Sim).  

/*
 * Maximize similarity rule: Keep only the maximum similarity from item (I) to any cluster.
 */
sim(I, _, Sim1) \ sim(I, _, Sim2) <=> Sim2 =< Sim1 | calculatedSim(1).

/*
 * Keep correct count for how many times the maximize similarity rule has been applied.
 */
true \ calculatedSim(X), calculatedSim(Y) <=> Sum is X + Y | calculatedSim(Sum).

/*
 * Start assigning items to clusters once the number of times the maximize similarity rule has been applied
 ((CountClusters -1) * CountItems) times.
 */
countClusters(Cl), countItems(I)\ calculatedSim(S) <=> S is (Cl-1) * I | assign(0). 

/*
 * By now the minimum distance from item (I) to any cluster is found.
 * Append (I)'s item vector and id to the cluster (C)'s lists, and count that one assignment has been made  
 */
itemVector(I, V) \ assign(A), sim(I, C, _), cluster(C, Cent, Ids, Vecs) 
					<=> append([I], Ids, NewIds), append([V], Vecs, NewVecs), A1 is A + 1 
						| cluster(C, Cent, NewIds, NewVecs), assign(A1).
						
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Move centroid step

/*
 * All items have been assigned to their closest clusters, increment the current iteration, and start
 counting number of centroids moved from 0. 
 */
countItems(A), iterate(I) \ assign(A), currentIteration(It) 
							<=> It < I, NewIt is It+1 | move(0), currentIteration(NewIt).
/*
 * Stop moving centroids once all clusters have had their centroids moved.
 */
countClusters(K)\ move(K) <=> true.

/*
 * Compute the average of the item vectors for each cluster to replace the cluster centroid, 
 and increment number of moved centroids each time
 */
true \ cluster(C, _, _, Vecs), move(X)
		<=> computeAverage(Vecs, Avg), X1 is X+1 | cluster(C, Avg, [], []), move(X1).