:- use_module(library(chr)).

:- chr_constraint 	userTaggedItem/2, link/3.

userTaggedItem(U, I) \ userTaggedItem(U, I) <=> true.

userTaggedItem(U, I1) \ userTaggedItem(U, I2)  <=> I1 @< I2 | link(I1, I2, 1), 
		open('links.txt',append, Stream), write(Stream, I1), write(Stream, ':'), write(Stream, I2), nl(Stream),
		close(Stream).