:- use_module(library(chr)).

:- chr_constraint start/0,  ann/3, item/1, tag/1, tag/2,
							countItems/1, countTags/1, 
							tagVector/1, itemVector/2, 
							countClusters/1, countCentroids/1, possibleCentroid/1,
							cluster/4,
							itemInCluster/2, distList/2, countItemDist/2, itemDistDone/1, linksNotChecked/1,
							assign/1, itemsAssignDone/1, assignStepDone/0,
							move/1, moveClusters/1, iterate/1, currentIteration/1,
							mustLink/3, cannotLink/3, 
							ml/3, cl/3, tempLinks/2, numLinks/2, totalLinks/1.

/* Notes: 
 * Same code as "KMeans-Tagging", with the addition of instance level constraints (Must-Link & Cannot-Link)
 * Minimum number of clusters is 2. 
 * Must input the following:
 * 1. iterate(X):- represents how many iterations kmeans will run
 * 2. totalLinks(X):- the number of must/cannot links input by user.
 * Output is the cluster(Id, Centroid, ItemIds, ItemVectors) constraint.
 * 
 * Query:
 * ann(maged, i1, sports), ann(maged, i1, ball), ann(khaled, i1, sports), ann(khaled, i1, football),
 ann(maged, i2, sports),ann(maged, i2, football),ann(khaled, i2, football),ann(khaled, i2, match),
 ann(maged, i3, cinema),ann(maged, i3, movie),ann(khaled, i3, cinema),ann(khaled, i3, film),
 ann(maged, i4, cinema),ann(maged, i4, film),ann(khaled, i4, film),ann(khaled, i4, comedy), ann(khaled,i4,movie),
 ann(maged, i5, nature),ann(maged, i5, forest),ann(khaled, i5, cinema),ann(khaled, i5, nature),
 ann(maged, i6, trees),ann(maged, i6, forest),ann(khaled, i6, nature),ann(khaled, i6, trees), 
 start, countClusters(3), iterate(1), 
 cannotLink(i1, i6,1), mustLink(i3, i5, 1), totalLinks(2).
 */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PROLOG

infValue(10).

/*
 * fillZeroes(L, Cnt) ==> Fill list L with #Cnt zeroes.
 */
fillZeroes([0],1).
fillZeroes([0|T], Cnt):- NewCnt is Cnt -1, fillZeroes(T, NewCnt). 

/*
 * increment(L, Idx, NewList) ==> Increment L at position Idx by 1 (L is 1-based)
 */
increment([H|T], 1, [NewH|T]) :- NewH is H+1.
increment([H|T], Idx, [H|NewT]):- Idx > 1, NewIdx is Idx-1, increment(T, NewIdx, NewT). 

/*
 * cosineSim(V1, V2, Sim) ==> Calculates cosine similarity between V1 & V2
 */
cosineSim(V1, V2, Sim):- dotProduct(V1, V2, P), sumSquare(V1, S1), sumSquare(V2, S2),
							sqrt(S1, M1), sqrt(S2, M2), Sim is P/(M1*M2).

/*
 * dotProduct(V1, V2, P) ==> Calculates dot product of vectors V1 & V2
 */
dotProduct([],[],0).
dotProduct([H1|T1], [H2|T2], P):- dotProduct(T1, T2, NewP), Cur is H1 * H2, P is Cur + NewP.
 
/*
 * sumSquare(V, M) ==> Calculates the sum of the squares of elements in vector V.
 */
sumSquare([], 0).
sumSquare([H|T], S):- sumSquare(T,NewS), Cur is H*H, S is Cur + NewS.

/*
 * computeAverage(L, Avg) ==> Returns a list which is the average of the list of lists L.
 * Calls sum, which returns a list containing the sum of all elements at the same index,
 then it calls average, which divides all items in Sum by CntItems 
 */

computeAverage(List, Avg):- List = [H|_], length(H, CntTags), sum(List, 0, CntTags, [], Sum), 
							length(List, CntItems), average(Sum, [], Avg, CntItems). 

/*
 * sum(List, Idx, Len, Temp, Avg) ==> Returns a list (Avg) containing sum of all elements at the same index.
 * Loops from 0 till Len, calling sumAtIndex in every iteration, which returns an element representing the
 sum of all elements at the same index in the list of lists (List). 
 */

sum(_, Len, Len, Sum, Sum).
sum(List, Idx, Len, Temp, Sum):- Idx<Len,sumAtIndex(List, Idx, S), append(Temp, [S], NewTemp),
								NewIdx is Idx+1, sum(List,NewIdx,Len,NewTemp,Sum).

/*
 * sumAtIndex([List|Tail], Idx, Sum):- Returns the sum (Sum) of all elements at the same index (Idx).
 * Keeps calling nth0 on each list in the list of lists until there are no more.
 */
sumAtIndex([], _, 0).
sumAtIndex([List|Tail], Idx, Sum):- nth0(Idx, List, Cur), sumAtIndex(Tail, Idx, Rem), Sum is Cur + Rem.

/*
 * average([H|T], Temp, Avg, CntItems):- Divides all items in [H|T] by CntItems.
 */
average([], Avg, Avg, _).
average([H|T], Temp, Avg, CntItems):- Cur is H/CntItems, append(Temp, [Cur], NewTemp), 
									average(T, NewTemp, Avg, CntItems). 
									
/*
 * allDifferent(A, B, C):- Checks that all three items are different
 */
allDifferent(A, B, C):- A\=B, B\=C, A\=C.

/*
 * minimum(A, B, Min):- Min is the minimum of A & B.
 */
minimum(A, B, A):- A<B.
minimum(A, B, B):- B=<A.

/*
 * clusterInList(C, L):- Cluster with ID (C) is in list (L).
 */

clusterInList(C, [[_,C]|_]).
clusterInList(C, [[_,C1]|Tail]):- C \= C1, clusterInList(C, Tail).

/*
 * clusterNotInList(C, L):- Cluster with ID (C) is not in list (L).
 */

clusterNotInList(_, []).
clusterNotInList(C, [[_,T]|Tail]):- C\=T, clusterNotInList(C,Tail).

/*
 * deleteDist(C, L, List):- Delete cluster with ID (C) from L
 */

%deleteDist(_, [], []).
deleteDist(C, [[_,C]|Tail], Tail).
deleteDist(C, [[H,T]|Tail], List):- C\=T, deleteDist(C, Tail, Rem), append([[H,T]], Rem, List).

/*
 * replaceClusterDist(L, C, Val, NewL):- Replace value of cluster (C) with (Val)
 */

replaceClusterDist([[_,C]|Tail], C, Val, [[Val,C]|Tail]).
replaceClusterDist([[H,T]|Tail], C, Val, NewL):- 
			C\=H, replaceClusterDist(Tail, C, Val, TempL), append([[H,T]], TempL, NewL).

/*
 * generateClusterIDs(K, [], List):- Generate list of ids from 1 to K in List
 */

generateClusterIDs(0, List, List).
generateClusterIDs(K, Temp, List):- append(Temp, [K], NewTemp), K1 is K-1, generateClusterIDs(K1, NewTemp, List).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Generate item vectors step

/*
 * Start counting the number of currentIterations from 1.
 */
iterate(_) ==> currentIteration(1).

/*
 * Generate an empty vector of tags in the beginning
 */
start <=> tagVector([]).

/*
 * Generate all items and keep only one copy of each, and count the number of items.
 */
ann(_, I, _) ==> item(I).
item(I) \ item(I) <=> true.
item(_) ==> countItems(1).
true \ countItems(X), countItems(Y) <=> Sum is X + Y | countItems(Sum).

/*
 * Generate all tags and keep only one copy of each
 */
 ann(_, _, T)  ==> tag(T).
 tag(T) \ tag(T) <=> true.
 
/*
 * Insert each tag in the tag vector, and give it an id which is its position in the vector.
 */
true \ tag(T), tagVector(L) <=> append(L, [T], NewList), length(NewList, Id) | tag(Id, T), tagVector(NewList).

/*
 * Count number of unique tags
 */
tagVector(Vector) ==> length(Vector, Cnt), countTags(Cnt).

/*
 * Generate a zero-filled tag vector for all items in the beginning
 */
item(I), countTags(Cnt) ==> fillZeroes(L, Cnt) | itemVector(I, L).

/*
 * For each annotation, increment the item vector at the position of the tag by 1.
 * Must remove ann as not to keep applying the rule for each new itemVector
 * Could replace ann by another constraint at beginning to preserve
 */
tag(Id, T) \ ann(_, I, T), itemVector(I, L) <=> increment(L, Id, NewList) | itemVector(I, NewList).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Compute transitive closure of instance level constraints + count constraints

/* Third feature in the instance level constraints represents how many occurrences of this constraint were added,
 assuming that users are the ones adding the constraints. Noisy constraints should be removed; 
 ones that occur a very small number of times.
 */

/*
 * Transitivity of must link: A-B, B-C ==> A-C
 */
mustLink(A, B, Cnt1), mustLink(B, C, Cnt2) 
		==> allDifferent(A,B,C), minimum(Cnt1, Cnt2, Cnt)  | mustLink(A, C, Cnt), totalLinks(1).

/*
 * Transitivity of cannot link: A-B, B/C ==> A/C
 */
mustLink(A, B, Cnt1), cannotLink(B, C, Cnt2)  
		==> allDifferent(A,B,C), minimum(Cnt1, Cnt2, Cnt) | cannotLink(A, C, Cnt), totalLinks(1).

/*
 * Can't have both mustLink(A,B) & cannotLink(A,B).
 * TO-DO: Possible to remove constraint which occurs significantly less.
 */
mustLink(A, B, _), cannotLink(A, B, _) ==> false.

/*
 * Count number of links for each item and keep correct count.
 Keep correct count of number of total links (to mark that assign step should start).
 */
 item(I) ==> numLinks(I, 0).
 cannotLink(A, B, _) ==> numLinks(A, 1), numLinks(B,1), totalLinks(-1).
 mustLink(A, B, _) ==> numLinks(A, 1), numLinks(B, 1), totalLinks(-1).
 true \ numLinks(I, Cnt1), numLinks(I, Cnt2) <=> Sum is Cnt1 + Cnt2 | numLinks(I, Sum).
 true \ totalLinks(Cnt1), totalLinks(Cnt2) <=> Sum is Cnt1 + Cnt2 | totalLinks(Sum).
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Generate centroids step

/*
 * Start counting number of initial centroids from 1.
 */
countClusters(_) ==> countCentroids(1).

/*
 * Each item is a possible centroid at the start.
 */
item(I) ==> possibleCentroid(I).

/*
 * Initiate a cluster using one of the possible centroids, and increment count of initial centroids by one.
 * Each cluster is initially empty, and its centroid is the item vector of the possible centroid.
 */
countClusters(Cl), itemVector(I, L) \ possibleCentroid(I), countCentroids(Cn) <=> 
				Cn =< Cl, Cnew is Cn +1 | cluster(Cn, L, [], []), countCentroids(Cnew).

/*
 * Remove remaining possible of centroids once all needed clusters have been initiated.
 */
countClusters(Cl), countCentroids(Cn) \ possibleCentroid(_) <=>
				Cn > Cl | true.

/*
 * Remove count of initial centroids once all clusters have been initiated (From 1 to CountClusters)
 * This prevents this section of rules to be applied more than once.
 */
countClusters(Cl) \ countCentroids(Cn) <=> Cn > Cl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Assign to cluster step (using distance list)

/*
 * Note: Have to assign one by one to check instance level constraints, 
 unlike the regular K-Means where similarities were calculated for all, then all were assigned at once.
 */


/*
 * Generate an empty list of similarities (Sim between item (I) to all clusters).
 * Mark number of distances computed to be 0. 
 * Start counting number of instance links checked.
 */
currentIteration(_), item(I), numLinks(I, Cnt), totalLinks(0) 
					==> distList(I, []), countItemDist(I, 0), tempLinks(I, Cnt).

/*
 * Generate temporary instance links for each iteration.
 */
currentIteration(_), cannotLink(I1, I2, Cnt) ==> cl(I1, I2, Cnt).
currentIteration(_), mustLink(I1, I2, Cnt) ==> ml(I1, I2, Cnt).

/*
 * If cl(I, I2) (or vice versa), and (I2) belongs to cluster (C), and distance from (I) to (C) hasn't been computed,
 add distance equal to (infValue) * (cntLinks).
 * Mark that one distance to cluster has been computed, update the distList, and remove the cl(I,I2) reducing 
 count of remaining instance links to be checked
 */
cluster(C, _, [], []), countItemDist(I,_), itemInCluster(I2, C) \ cl(I, I2, CntLinks), distList(I, L) 
	<=> clusterNotInList(C, L), infValue(Val), Inf is CntLinks*Val, append(L,[[Inf,C]],NewL) 
				| countItemDist(I, 1), distList(I, NewL), tempLinks(I, -1).

cluster(C, _, [], []), countItemDist(I,_), itemInCluster(I2, C) \ cl(I2, I, CntLinks), distList(I, L) 
	<=> clusterNotInList(C, L), infValue(Val), Inf is CntLinks*Val, append(L,[[Inf,C]],NewL) 
				| countItemDist(I, 1), distList(I, NewL), tempLinks(I, -1).

/*
 * If cl(I, I2) (or vice versa), and (I2) belongs to cluster (C), and distance from (I) to (C) was computed,
 change distance to (C) to be equal to (infValue) * (cntLinks).
 * Update the distList, and remove the cl(I,I2) reducing count of remaining instance links to be checked
 */
cluster(C, _, [], []), itemDistDone(I), itemInCluster(I2, C) 
	\ cl(I, I2, CntLinks), distList(I, L) 
	<=> clusterInList(C, L), infValue(Val), Inf is CntLinks*Val, replaceClusterDist(L, C, Inf, NewL) 
				| distList(I, NewL), tempLinks(I, -1).

cluster(C, _, [], []), itemDistDone(I), itemInCluster(I2, C) 
	\ distList(I, L), cl(I2, I, CntLinks) 
	<=> clusterInList(C, L), infValue(Val), Inf is CntLinks*Val, replaceClusterDist(L, C, Inf, NewL) 
				| distList(I, NewL), tempLinks(I, -1).
%%%%%

/*
 * If ml(I, I2) (or vice versa), and (I2) belongs to cluster (C), and distance from (I) to (C) hasn't been computed,
 add distance equal to -1 * (infValue) * (cntLinks). Negative distance signifies mustLink.
 * Mark that one distance to cluster has been computed, update the distList, and remove the cl(I,I2) reducing 
 count of remaining instance links to be checked
 */
cluster(C, _, [], []), countItemDist(I,_), itemInCluster(I2, C) \ ml(I, I2, CntLinks), distList(I, L) 
	<=> clusterNotInList(C, L), infValue(Val), Inf is -1 * CntLinks*Val, append(L,[[Inf,C]],NewL)
				| countItemDist(I, 1), distList(I, NewL), tempLinks(I, -1).

cluster(C, _, [], []), countItemDist(I,_), itemInCluster(I2, C) \ ml(I2, I, CntLinks), distList(I, L) 
	<=> clusterNotInList(C, L), infValue(Val), Inf is -1 * CntLinks*Val, append(L,[[Inf,C]],NewL) 
				| countItemDist(I, 1), distList(I, NewL), tempLinks(I, -1).

/*
 * If ml(I, I2) (or vice versa), and (I2) belongs to cluster (C), and distance from (I) to (C) hasn't been computed,
 add distance equal to -1 (infValue) * (cntLinks).
 * Update the distList, and remove the cl(I,I2) reducing count of remaining instance links to be checked
 */
cluster(C, _, [], []), itemDistDone(I), itemInCluster(I2, C) 
	\ ml(I, I2, Cnt), distList(I, L) 
	<=> clusterInList(C, L), infValue(Val), Inf is -1 * Cnt*Val, replaceClusterDist(L, C, Inf, NewL) 
				| distList(I, NewL), tempLinks(I, -1).

cluster(C, _, [], []), itemDistDone(I), itemInCluster(I2, C) 
	\ distList(I, L), ml(I2, I, Cnt) 
	<=> clusterInList(C, L), infValue(Val), Inf is -1 * Cnt*Val, replaceClusterDist(L, C, Inf, NewL) 
				| distList(I, NewL), tempLinks(I, -1).

/*
 * I2 is not asssigned yet (linksNotChecked constraint still not removed), so ignore cl/ml(I, I2).
 */
linksNotChecked(I2), cl(I, I2, _), tempLinks(I, Cnt) ==> Cnt > 0 | tempLinks(I, -1).
linksNotChecked(I2), ml(I, I2, _), tempLinks(I, Cnt) ==> Cnt > 0 | tempLinks(I, -1).

/*
 * Keep correct count of number of instance links checked.
 */
true \ tempLinks(I, Cnt1), tempLinks(I, Cnt2) <=> Sum is Cnt1 + Cnt2 | tempLinks(I, Sum). 

/*
 * Add distance from item (I) to cluster (C) to the list of distances (L).
 */
itemVector(I, L1), cluster(C, L2, [], []), countItemDist(I,_) \ distList(I, L) <=> 
			 clusterNotInList(C, L), cosineSim(L1, L2, Sim), Distance is 1-Sim, append(L,[[Distance,C]],NewL)
					| countItemDist(I, 1), distList(I, NewL).
				
/*
 * Keep count of how many distances to cluster have been computed for item (I).
 */				
true \ countItemDist(I, Cnt1), countItemDist(I, Cnt2) <=> Cnt is Cnt1 + Cnt2 | countItemDist(I, Cnt).

/*
 * Mark item as done (distances computed) once number of distances to cluster computed 
 is equal to the number of clusters.
 * Sort the distance list (will be resorted after instance links are checked).
 * Mark item as having its links still not checked.
 */
countClusters(Clusters) \ countItemDist(I, Clusters), distList(I, L) <=> 
								sort(L, NewL) | itemDistDone(I), linksNotChecked(I), distList(I, NewL).

/*
 * All instance links have been checked, so sort the distance list.
 */
tempLinks(I, 0) \ linksNotChecked(I), distList(I, L) <=> sort(L, NewL) | distList(I, NewL).

/*
 * Keep only minimum distance in the distance list once all distances have been computed (itemDistDone(I))
 and all constraints have been checked (tempLinks(I,.
 */ 
itemDistDone(I)\ tempLinks(I, 0), distList(I, [[Dist,Cluster]|T]) <=> T\=[] | distList(I, [[Dist,Cluster]]).

/*
 * Once there is only one entry in the distance list, assign item (I) to cluster (C) and mark that one item has been
 assigned.
 */
true \ distList(I, [[_,C]]), itemDistDone(I) <=>  itemInCluster(I, C),  assign(1).

/*
 * Keep count of how many items have been assigned (to the constraint itemInCluster).
 */
true \ assign(X), assign(Y) <=> Sum is X+Y | assign(Sum).

/*
 * By now the minimum distance from each item to a cluster is found.
 * Append (I)'s item vector and id to the cluster (C)'s lists, and count that one assignment has been made  
 */
currentIteration(It), itemVector(I, V), countItems(Cnt), assign(Cnt) \ itemInCluster(I, C), cluster(C, Cent, Ids, Vecs) 
					<=> append([I], Ids, NewIds), append([V], Vecs, NewVecs)
					%, print(It + ' ' + C + ' ' + NewIds), nl
						| cluster(C, Cent, NewIds, NewVecs), itemsAssignDone(1).

/*
 * Keep count of how many items have been assigned to the clusters' lists.
 */
true \ itemsAssignDone(X), itemsAssignDone(Y) <=> Sum is X + Y | itemsAssignDone(Sum).

/*
 * Mark assign step to be done once all items have been assigned.
 */
countItems(A) \ assign(A), itemsAssignDone(A) <=> assignStepDone.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Move centroid step

/*
 * All items have been assigned to their closest clusters, increment the current iteration, and start
 counting number of centroids moved from 0. 
 */
iterate(I), currentIteration(It), countClusters(K) 
		==> It < I, generateClusterIDs(K, [], List) | move(0), moveClusters(List).

/*
 * Compute the average of the item vectors for each cluster to replace the cluster centroid, 
 and increment number of moved centroids each time
 */
assignStepDone \ cluster(C, _, _, Vecs), move(X), moveClusters(List)
		<=>  member(C, List), computeAverage(Vecs, Avg), X1 is X+1, delete(List, C, NewList) 
			| cluster(C, Avg, [], []), move(X1), moveClusters(NewList).

assignStepDone, cluster(_, _, [], []) \ move(X), moveClusters(List) 
		<=>  member(C, List), delete(List, C, NewList), X1 is X+1 | move(X1), moveClusters(NewList).
		
/*
 * Stop moving centroids once all clusters have had their centroids moved, and increment current iteratoin.
 */
countClusters(K)\ move(K), moveClusters([]), currentIteration(It), assignStepDone 
		<=> NewIt is It +1 | currentIteration(NewIt).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHR: Finished iterating

iterate(I), assignStepDone, currentIteration(I), cluster(C, _, Ids, _)
			==>  open('clusters.txt', append, Stream), write(Stream, C), write(Stream, ': '), write(Stream, Ids),
					%print(', '), print(Cent), print(', '), print(Vecs), 
						nl(Stream), close(Stream).