% Author:
% Date: 03/03/2014

:- use_module(library(chr)).

:- chr_constraint   annotation/3, tag/1, item/1, link/3, itemTag/3,
					cosineNum/3, cosineDenItem/2, cosineDen/3,
					cosineSim/3,
 					startClustering/0,
 					currentCluster/1, distance/3, seed/2, itemInCluster/2.

/*annotation(maged, item1, sports).
annotation(maged, item1, ball).
annotation(maged, item2, movie).
annotation(maged, item2, cinema).
annotation(khaled, item1, sports).
annotation(khaled, item1, football).
annotation(khaled, item2, cinema).
startClustering ==> annotation(_,_,T) | tag(T).*/

countClusters(2).
linkValue(1).
distanceMeasure('co-occurence').



annotation(_,I,T) ==> tag(T), item(I), itemTag(I,T,1).
tag(T) \ tag(T) <=> true.
item(I) \ item(I) <=> true.
tag(T), annotation(_,I,_) ==> itemTag(I,T,0).
true \ itemTag(I,T,W1), itemTag(I,T,W2) <=> W is W1 + W2 | itemTag(I,T,W). 

itemTag(I1, T, W1), itemTag(I2, T, W2) ==> I1 @<I2, W is W1 * W2 | cosineNum(I1, I2, W).
cosineNum(I1, I2, W1), cosineNum(I1, I2, W2) <=> W is W1 + W2 | cosineNum(I1, I2, W).

itemTag(I, _ , W) ==> Weight is W*W | cosineDenItem(I, Weight).
true \ cosineDenItem(I, W1), cosineDenItem(I, W2) <=> W is W1 + W2 | cosineDenItem(I, W).

cosineNum(I1, I2, Num), cosineDenItem(I1, Den1), cosineDenItem(I2,Den2) 
		<=> Den is sqrt(Den1) * sqrt(Den2), Den > 0, Sim is Num/Den | cosineSim(I1, I2, Sim).

startClustering <=> currentCluster(0).

% Random assignment of tags to be initial seeds.
%%NOTE: Very easy using CHR to choose the initial seeds yourself, just specify the constraint instead of using the above rule!
currentCluster(X) <=> countClusters(X) | true.
true \ item(I), currentCluster(X) <=> X1 is X+1 | seed(I, X), currentCluster(X1).


%Computing distance of remaining tags to seeds. Specify distance measure method using distanceMeasure fact.
seed(Seed, _), tag(Tag), link(Seed,Tag,Weight) ==> distanceMeasure('co-occurence'),
									Distance is 1/Weight | distance(Tag, Seed, Distance).

%Keeping minimum distance only
distance(Item,_, W1) \ distance(Item,_,W2) <=> W1 =< W2 | true.

%Assigning items to clusters when done
distance(Item, Seed, _), seed(Seed, Cluster) ==> itemInCluster(Item, Cluster).

%Assigning seeds to clusters as well in the end)

seed(Seed , Cluster) ==> itemInCluster(Seed, Cluster).
