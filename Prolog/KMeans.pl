:- use_module(library(chr)).

:- chr_constraint point/3, centroid/3,
					countClusters/1, countPoints/1,
					countCentroids/1, possibleCentroid/2,
					distanceToCentroid/3, pointInCluster/2, 
					calculatedDistance/1, pointsInCluster/2, 
					startClustering/0, assignStepDone/0,
					clusterSum/4, clusterAverage/3, pointForSum/3,
					newCentroids/1,
					iteration/1, totalIterations/1.

/* (countCentroids) is for the selection of the first centroids from the available points. Keeps count of remaining
 * Keeps count of remaining centroids to be selected.
 * (possibleCentroid) is the list of all possible centroids for the baseCentroids (chosen to be all initial points).
 */

/* Queries
 * point(1,1,1), point(2,1,2), point(3,3,3), point(4,3,4), countClusters(2).
 * point(1,1,1), point(2,1,2), point(3,6,6), point(4,6,7), point(5,2,2), point(6,6,8), countClusters(2), totalIterations(2).
 * point(1,1,1), point(2,1,2), point(3,6,6), point(4,6,7), point(5,5,5), point(6,6,8), countClusters(2), totalIterations(2).
 */

/* Distance chosen to be summation of squares of differences between coordinates. 
 *(Add sqrt and this becomes euclidean distance).
 */
distance(X1, Y1, X2, Y2, Dist):- Diff1 is X1 - X2, Diff2 is Y1 - Y2,
								Sq1 is Diff1 * Diff1, Sq2 is Diff2*Diff2,
								Dist is Sq1 + Sq2.
								
/*
 * Each point is a possible centroid in the first run of kmeans.
 * Each point counts as 1 to be able to count them later.
 */								
point(_, X, Y) ==> possibleCentroid(X, Y), countPoints(1).

/*
 * Count the number of points as not to need to be input by user in the query.
 */
true \ countPoints(X), countPoints(Y) <=> Sum is X+Y | countPoints(Sum).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% START OF GENERATE INITIAL CENTROIDS STEP

/*
 * Start counting centroids getting generated from 1.
 */
countClusters(_) ==> countCentroids(1), iteration(1).

/*
 * Remove all remaining possible centroids once the number of needed centroids (one for each cluster) = clusters.
 */
countClusters(X), countCentroids(Cnt) \ possibleCentroid(_,_) <=> Cnt is X+1 | true.

/*
 * No need for this constraint anymore.
 */
countClusters(X) \ countCentroids(Cnt) <=> Cnt is X+1 | startClustering, true.

/* One of the possible centroids is selected to be a centroid. 
 * Idx of centroids incremented by one.
 */
true  \ possibleCentroid(X,Y), countCentroids(CntCentroids) 
					<=> NewCnt is CntCentroids + 1 | centroid(CntCentroids, X, Y), countCentroids(NewCnt).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF GENERATE INITIAL CENTROIDS STEP

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% START OF ASSIGN TO CLUSTER STEP

/* Generate distance from all points to all centroids.
 * For cannot link constraints, add rule to simply not generate/simplify/remove the distanceToCentroid constraint
 * For must link constrains, add rule to simply assign to cluster right away
 * Beware though, must keep correct count to know when assign to cluster step has finished (calculatedDistance constraint).
 */
startClustering, point(PIdx, Xp, Yp), centroid(CIdx, Xc, Yc) 
		==> distance(Xp, Yp, Xc, Yc, Dist) | distanceToCentroid(PIdx, CIdx, Dist).

/*
 * This rule will have all points be assigned to all clusters.
 * Another rule will be responsible for keeping only the minimum distance.
 * This was done as not to remove the "point" constraint. 
 */
distanceToCentroid(PIdx, CIdx, _) ==> pointInCluster(PIdx, CIdx).

/*
 * Minimize distance rule.
 * Keep only the minimum distance from the point to any cluster.
 * Generate a calculatedDistance(1) to indicate that the rule has been applied once.
 * Assign to cluster step for all points will finish once this rule has been applied (Clusters-1) * Points times
 * Will need more rules for constrained clustering (must-link/cannot-link/size of cluster).
 */
distanceToCentroid(PIdx, CIdx1, Dist1) \ distanceToCentroid(PIdx, CIdx2, Dist2),pointInCluster(PIdx,CIdx2) 
					<=> CIdx1 \= CIdx2, Dist1 < Dist2 | calculatedDistance(1).

/*
 * Keep track of how many times the minimize distance rule has been applied
 */
true \ calculatedDistance(X), calculatedDistance(Y) <=> Sum is X+Y | calculatedDistance(Sum).

/*
 * Mark the assign step to be done once the Minimize Distance rule has been applied (Clusters-1) * Points time.
 */
countClusters(CntClusters), countPoints(CntPoints),calculatedDistance(Total), iteration(It), totalIterations(TotalIt) 
	==> Total is (CntClusters-1) * CntPoints, It < TotalIt | assignStepDone.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF ASSIGN TO CLUSTER STEP

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% START OF MOVE CENTROIDS STEP 


assignStepDone \ startClustering <=> true.

/*
 * Generate a count for number of points in each cluster once the cluster assignment step has finished.
 * Count needed to be able to compute average.
 */
assignStepDone,	pointInCluster(PIdx, CIdx), point(PIdx, _, _), centroid(CIdx, _,  _)  
  				==> pointsInCluster(CIdx, 1).
  
/*
 * Sum up number of points in each cluster.
 */  			
true \ pointsInCluster(CIdx, Cnt1), pointsInCluster(CIdx, Cnt2) 
				<=> Cnt is Cnt1 + Cnt2 | pointsInCluster(CIdx, Cnt).

/*
 * For each cluster, initialize its sum to 0
 */
assignStepDone, centroid(CIdx, _, _) ==> clusterSum(CIdx, 0, 0, 0).

/*
 * For each point, to preserve its constraint, generate a "pointForSum" constraint for the sake of the average step.
 */
assignStepDone, point(PIdx, Xp, Yp) ==> pointForSum(PIdx, Xp, Yp).

/*
 * Add the value of each point in the same cluster to "clusterSum"
 * Also increment the number of points summed up in this cluster
 */
pointInCluster(PIdx,CIdx) \ pointForSum(PIdx, Xp, Yp), clusterSum(CIdx, OldCnt, OldX, OldY)
	<=> NewX is OldX + Xp, NewY is OldY + Yp, NewCnt is OldCnt+1 | clusterSum(CIdx, NewCnt, NewX, NewY).

/*
 * Calculate cluster average once all points in cluster have been summed up
 */

clusterSum(CIdx, Cnt, X, Y), pointsInCluster(CIdx, Cnt) ==> AvgX is X/Cnt, AvgY is Y/Cnt | clusterAverage(CIdx, AvgX, AvgY).

/*
 * Once the number of averages calculated is equal to the number of clusters, do the following:
 * 1. Remove all assignments of points to cluster.
 * 2. Remove all previous centroids.
 * 3. NOT DONE: Increment number of runs KMeans has run (if assumed that stopping condition is fixed number of iterations).
 * 4. Reset calculatedDistance constraint (count of number of times Minimize Distance constraint has been applied).
 * 5. Remove all distanceToCentroid constraints.
 * 6. Create new centroids and keep track of their count.
 * 7. Once all centroids have been moved, start cluster step again.
 */

clusterAverage(CIdx, _, _) \ pointInCluster(_, CIdx) <=> true.

clusterAverage(CIdx, _, _) \ pointsInCluster(CIdx, _) <=> true.

clusterAverage(CIdx, _, _) \ centroid(CIdx, _, _) <=> true.

clusterAverage(_, _, _) \ calculatedDistance(_) <=> true.

clusterAverage(CIdx, _, _) \ distanceToCentroid(_, CIdx, _) <=> true.

assignStepDone \ clusterSum(CIdx, _, _, _), clusterAverage(CIdx, Xc, Yc) 
		<=> centroid(CIdx, Xc, Yc), newCentroids(1).
		
true \ newCentroids(X), newCentroids(Y) <=> Sum is X+Y | newCentroids(Sum).

countClusters(Cnt)\ assignStepDone, newCentroids(Cnt)  <=> nl, print("Start Clustering"), iteration(1).

true \ iteration(X), iteration(Y) <=> Sum is X+Y | iteration(Sum). 	

iteration(It), totalIterations(Total) ==> It =< Total | startClustering.
