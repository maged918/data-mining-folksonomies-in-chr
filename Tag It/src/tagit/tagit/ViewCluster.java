package tagit.tagit;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

import org.json.JSONArray;

import tagit.tagit.clustering.Photo;
import tagit.tagit.util.DownloadImage;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.DownloadPhotos;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.ImageStorage;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class ViewCluster extends Activity {
	
	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	public ArrayList<ImageItem> imageItems;
	public String httpResponse;
	public JSONArray jsonArr;
	public Photo[] photos;
	public Context context;
	public int constrained;
	public double[] uncertainty;
	public int clusterId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_cluster);
		
		getPhotoIds();
		constrained = Integer.parseInt(getIntent().getStringExtra("constrained"));
		clusterId = Integer.parseInt(getIntent().getStringExtra("clusterId"));
		
		saveUncertainty();
		
		gridView = (GridView) findViewById(R.id.gridViewPhotosInCluster);
		//downloadPhotos();
		DownloadPhotos d = new DownloadPhotos(this, photos, imageItems, gridView);
		d.download();
	}
	
	public void getPhotoIds(){
		Intent in = getIntent();
		String s = in.getStringExtra("ids");
		StringTokenizer stIds = new StringTokenizer(s, ",");
		s = in.getStringExtra("titles");
		StringTokenizer stTitles = new StringTokenizer(s, ",");
		s= in.getStringExtra("urls");
		StringTokenizer stUrls = new StringTokenizer(s, ",");
		s = in.getStringExtra("uncertainty");
		StringTokenizer stUnc = new StringTokenizer(s,",");
		photos = new Photo[stIds.countTokens()];
		uncertainty = new double[stIds.countTokens()];
		for(int i=0; i<photos.length; i++){
			//TODO: Remove this hack
			if(stIds.hasMoreTokens() && stTitles.hasMoreTokens() && stUrls.hasMoreTokens()){
				photos[i] = new Photo(
						Integer.parseInt(stIds.nextToken()), stTitles.nextToken(), stUrls.nextToken());
				uncertainty[i] = Double.parseDouble(stUnc.nextToken());
			}
		}
		Log.d("Cluster Photo Ids", Arrays.toString(photos));
	}
	
	public void saveUncertainty(){
		for(int i=0; i<photos.length; i++){
			DownloadJSON task = new DownloadJSON(){
				public void receiveData(String s){
					
				}
			};
			task.execute(new String[]{getString(R.string.base_address)+"save_clusters.php"});
		}
	}

}
