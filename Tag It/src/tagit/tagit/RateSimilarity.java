package tagit.tagit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.clustering.Photo;
import tagit.tagit.login.Login;
import tagit.tagit.util.DownloadImage;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.DownloadPhotos;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.ImageStorage;
import tagit.tagit.util.JSONParser;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RatingBar;

public class RateSimilarity extends Activity {
	
	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	public String httpResponse;
	public JSONArray jsonArr;
	public static Photo[] photos;
	public ArrayList<ImageItem> imageItems;
	public Context context;
	public ProgressDialog pDialog;
	
	public String address;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rate_similarity);
		//Log.d("User ID", Home.userId);
		gridView = (GridView)findViewById(R.id.gridViewRateSimilarity);
		address = getString(R.string.base_address)
				+"rate_similarity.php?user_id=" + Home.userId;
		context = this;
		DownloadPhotos d = new DownloadPhotos(address, context, photos, imageItems, gridView,
				DownloadPhotos.RATE_SIMILARITY_MODE);
		d.run();
	}
	
	public static void setPhotos(Photo[] arr){
		photos = arr;
	}
	
	public void setProgressDialog(int mode){
		pDialog = new ProgressDialog(RateSimilarity.this);
        pDialog.setTitle("Contacting Servers");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
		switch(mode){
		case 1:
            pDialog.setMessage("Downloading Photo Information..");
            break;
		case 2:
            pDialog.setMessage("Saving Similarity..");
            break;
		}
	}
	
	public void saveSimilarity(View v){
		RatingBar similarityBar = (RatingBar)findViewById(R.id.similarityBar);
		double similarity = similarityBar.getRating();
		Log.d("Save Similarity ", "Similarity = " + similarity );
		
		SaveSimilarity save = new SaveSimilarity();
		//Log.d("Save Similarity", photos[0].toString());
		save.execute(new String[]{getString(R.string.base_address)+
				String.format("save_similarity.php?user_id=%s&photo_1=%s&photo_2=%s&rating=%f&experiment_id=%d", 
						Home.userId,photos[0].getId(), photos[1].getId(),similarity, Home.experimentId)});
	}
	
	private class SaveSimilarity extends AsyncTask <String, Void, Integer>{
		
        protected void onPreExecute() {
            super.onPreExecute();
            imageItems = new ArrayList();
            
            setProgressDialog(2);
        }
		
		protected Integer doInBackground(String... urls) {
			Log.d("Do In Background", "Here!");
			try {
				InetAddress address = InetAddress.getByName(urls[0]);
			} catch (UnknownHostException e) {
				Log.d("DNS Error", e.getMessage());
			}
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(urls[0]);
			try{
				HttpResponse response = httpClient.execute(post);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				StringBuffer sb = new StringBuffer();
				while((line=br.readLine())!=null){
					sb.append(line);
					Log.d("HTTP Response ", line);
				}
				is.close();
				
				int status = Integer.parseInt(sb.toString());
				return status;
			} catch(IOException e){
				e.printStackTrace();
				Log.d("Query Rate Similarity", "Error from http response " + e.toString());
			}
			return 0;
		}
		
		protected void onPostExecute(Integer result){
			pDialog.dismiss();
			Toast.makeText(RateSimilarity.this, "Similarity Saved", 1000).show();
			if(result == 1){
				Log.d("On Post Execute", "Similarity saved");
				DownloadPhotos d = new DownloadPhotos(address, context, photos, imageItems, gridView,
						DownloadPhotos.RATE_SIMILARITY_MODE);
				d.run();
			}
		}
	}
}
