package tagit.tagit.clustering;

import java.util.ArrayList;
import java.util.Arrays;

public class Cluster {
	
	private int id;
	private ArrayList<Photo> photos;
	private double[] centroid;
	
	public Cluster(int id, int[] center){
		this.id = id;
		centroid = new double[center.length];
		for(int i=0; i<centroid.length; i++) centroid[i] = center[i];
		photos = new ArrayList<Photo>();
	}
	
	public int getSize(){
		return photos.size();
	}
	
	public ArrayList<Photo> getPhotos(){
		return photos;
	}
	
	public void addPhoto(Photo p){
		photos.add(p);
	}
		
	public void resetAssignments(){
		photos = new ArrayList<Photo>();
	}
	
	public void recomputeCentroid(){
		for(int i=0; i<centroid.length; i++){
			double sum = 0;
			for(int j=0; j<photos.size(); j++){
				sum+=photos.get(j).getTagCount(i);
			}
			if(photos.size()==0) centroid[i] = 0;
			else centroid[i] = sum/photos.size();	
		}
	}

	public double cosineSimilarity(int[] vector){
		double mag1 = magnitude(centroid);
		double mag2 = magnitude(vector);
		if(mag1==0) return 0;
		return dotProduct(centroid, vector)/(mag1*mag2);
	}
	
	public static double dotProduct(double[] centroid, int[] vector){
		double sum = 0;
		for(int i=0; i<centroid.length; i++) sum+= centroid[i]*vector[i];
		return sum;
	}
	
	public static double magnitude(int[] arr){
		double sum = 0;
		for(int i=0; i<arr.length; i++) sum+= arr[i]*arr[i];
		return Math.sqrt(sum);
	}
	
	public static double magnitude(double[] arr){
		double sum = 0;
		for(int i=0; i<arr.length; i++) sum+= arr[i]*arr[i];
		return Math.sqrt(sum);
	}
	
	public String toString(){
		return String.format("Cluster #%d, elements: %s", 
				id, printElementIds());
	}
	
	public String printElementIds(){
		String s = "";
		for(int i=0; i<photos.size(); i++){
			if(i!=0) s+=",";
			s+= photos.get(i).getId();
		}
		return s;
	}
	
	public String printElementTitles(){
		String s = "";
		for(int i=0; i<photos.size(); i++){
			if(i!=0) s+=",";
			s+= photos.get(i).getTitle();
		}
		return s;
	}
	
	public String printElementUrls(){
		String s = "";
		for(int i=0; i<photos.size(); i++){
			if(i!=0) s+=",";
			s+= photos.get(i).getUrl();
		}
		return s;
	}
	
	public String printElementUncertainty(){
		String s = "";
		for(int i=0; i<photos.size(); i++){
			if(i!=0) s+=",";
			double distance = cosineSimilarity(photos.get(i).getAllTags());
			double uncertainty = 1-distance;
			s+= uncertainty;
		}
		return s;
	}
	
}
