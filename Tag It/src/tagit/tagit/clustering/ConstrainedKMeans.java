package tagit.tagit.clustering;

import java.util.*;

import android.util.Log;

public class ConstrainedKMeans {
	
	static int iterations = 30;
	static int maxClusterSize = 30;
	static int k;
	static int n;
	
	static Photo[] photos;
	static HashMap<Integer, Integer> photoIds;
	static Constraint[] constraints;
	
	static HashSet<Integer> photosWithConstraints;
	static ArrayList<Constraint>[] mustLink;
	static ArrayList<Constraint>[] cannotLink;
	
	boolean[] visited;
	ArrayList<Integer> sameCluster;
	double componentSum;
	int componentSize;
	
	static Cluster[] clusters;
	static int photoInCluster[];
	
	public Cluster[] constrainedClustering(int countClusters, 
			Photo[] photosArray, Constraint[] constraintsArray){
		
		//System.out.println("In clustering constructor");
		
		k = countClusters;
		photos = photosArray;
		constraints = constraintsArray;
		n = photosArray.length;
		
		maxClusterSize = 450/k;
		
		mapPhotoIds();
		findPhotosInConstraints();
		constraints = computeTransitiveClosure();
		addConstraintsToPhotos();
		initiateClusters();
		
		for(int it=0; it<iterations; it++){
			//Log.d("Iteration", it+"");
			resetAssignments();
			for(int i=0; i<n; i++){
				double[] distance = computeDistanceToClusters(i);
				distance = checkConstraints(i, distance);
				int maxIdx = getMax(distance);
				assign(i, maxIdx);
			}
			recomputeCentroids();
		}
		
		//TODO: compute to what extent the clustering matches the constraints
		
		return clusters;
	}
	
	public void mapPhotoIds(){
		photoIds = new HashMap<Integer, Integer>();
		for(int i=0; i<n; i++){
			photoIds.put(photos[i].getId(), i);
		}
	}
	
	public void findPhotosInConstraints(){
		photosWithConstraints = new HashSet<Integer>();
		if(constraints == null) return;
		for(int i=0; i<constraints.length; i++){
			photosWithConstraints.add(photoIds.get(constraints[i].getP1()));
			photosWithConstraints.add(photoIds.get(constraints[i].getP2()));
		}
	}
	
	public Constraint[] computeTransitiveClosure(){
		//System.out.println("Photo ID of 0 " + photoIds.get(0));
		
		/*
		 * Creating an adjacency list for must link and cannot link constraints to be able to traverse them
		 * 	using DFS.
		 */
		
		mustLink = new ArrayList[n];
		cannotLink = new ArrayList[n];
		
		ArrayList<Constraint> newConstraints = new ArrayList<Constraint>();
		
		for(int i=0; i<n; i++) mustLink[i] = new ArrayList<Constraint>();
		for(int i=0; i<n; i++) cannotLink[i] = new ArrayList<Constraint>();
		
		if(constraints == null) return new Constraint[0];
		
		for(int i=0; i<constraints.length; i++){
			Constraint c = constraints[i];
			double similarity = c.getSimilarity();
			int p1 = c.getP1();
			int p2 = c.getP2();
			if(similarity>2.5){
				//int p1 = photoIds.get(c.getP1());
				//int p2 = photoIds.get(c.getP2());
				mustLink[photoIds.get(p1)].add(new Constraint(p1, p2, similarity));
				mustLink[photoIds.get(p2)].add(new Constraint(p2, p1, similarity));
			} else{
				cannotLink[photoIds.get(p1)].add(new Constraint(p1, p2, similarity));
				cannotLink[photoIds.get(p2)].add(new Constraint(p2, p1, similarity));
			}
		}
		visited = new boolean[n];
		Arrays.fill(visited, false);
		
		/*
		 * Finding must link connected components using DFS, 
		 * and adding a must link constraint between all pairs in the same component.
		 */
		
		for(int i=0; i<n; i++){
			if(photosWithConstraints.contains(i) && !visited[i]){
				sameCluster = new ArrayList<Integer>();
				componentSum = 0;
				componentSize = 0;
				//System.out.println("Node 1 visited " + visited[1]);
				dfs(i);
				//printMLSameCluster();
				double sim = componentSum / (componentSize-1);
				for(int j=0; j<sameCluster.size(); j++){
					for(int k=j+1; k<sameCluster.size(); k++){
						newConstraints.add(new Constraint
							(photos[sameCluster.get(j)].getId(), photos[sameCluster.get(k)].getId(), sim));
						newConstraints.add(new Constraint
								(photos[sameCluster.get(k)].getId(), photos[sameCluster.get(j)].getId(), sim));
					}
				}
			}
		}
		
		/*
		 * For all must link constraints, check if there are corresponding cannot links that can be inferred.
		 */
		
		int size = newConstraints.size();
		for(int i=0; i<size; i++){
			Constraint c = newConstraints.get(i);
			int p1 = photoIds.get(c.getP1());
			int p2 = photoIds.get(c.getP2());
			for(int j=0; j<cannotLink[p2].size(); j++){
				Constraint cl = cannotLink[p2].get(j);
				newConstraints.add(new Constraint(photos[p1].getId(), cl.getP2(), cl.getSimilarity()));
				newConstraints.add(new Constraint(cl.getP2(), photos[p1].getId(), cl.getSimilarity()));

			}
		}
		
		/*
		 * Add all original cannot link constraints.
		 */
		
		for(int i=0; i<n; i++){
			for(int j=0; j<cannotLink[i].size(); j++){
				newConstraints.add(cannotLink[i].get(j));
			}
		}
		
		Constraint[] arr = new Constraint[newConstraints.size()];
		for(int i=0; i<arr.length; i++)arr[i] = newConstraints.get(i);
		System.out.println(Arrays.toString(arr));
		return arr;
	}
	
	public void printMLSameCluster(){
		System.out.print("Nodes in the same cluster ");
		for(int j=0; j<sameCluster.size(); j++) System.out.print(sameCluster.get(j) + " ");
		System.out.println();
		System.out.println(componentSum + " " + componentSize);
	}
	
	public void dfs(int node){
		//System.out.println("Root " + node);
		visited[node] = true;
		sameCluster.add(node);
		componentSize++;
		for(int i=0; i<mustLink[node].size(); i++){
			int p2 = photoIds.get(mustLink[node].get(i).getP2());
			//System.out.println("Node " + p2 + " " + visited[p2]);
			//System.out.println(mustLink[node].get(i).getP2());
			if(!visited[p2]){
				//System.out.println("Not Visited " + p2);
				componentSum += mustLink[node].get(i).getSimilarity();
				dfs(p2);
			}
		}
	}
	
	public void addConstraintsToPhotos(){
		if(constraints!=null && constraints.length > 0){
			for(int i=0; i<constraints.length; i++){
				//Log.d("Adding Constraints to Photos", constraints[i].toString());
				photos[photoIds.get(constraints[i].getP1())].addConstraint(constraints[i]);
			}
		}
	}
		
	public void initiateClusters(){
		int[] used = new int[n];
		clusters = new Cluster[k];
		for(int i=0; i<k; i++){
			int idx = (int) (Math.random()*n);
			while(n > k && used[idx]!=0) idx = (int) (Math.random() *n);
			clusters[i] = new Cluster(i,photos[idx].getAllTags());
			used[idx] = 1;
		}
	}
	
	public void resetAssignments(){
		photoInCluster = new int[n];
		Arrays.fill(photoInCluster, -1);
		for(int i=0; i<k; i++){
			clusters[i].resetAssignments();
		}
	}
	
	public double[] computeDistanceToClusters(int idx){
		double[] distance = new double[k];
		for(int i=0; i<k; i++){
			distance[i] = clusters[i].cosineSimilarity(photos[idx].getAllTags());
		}
		return distance;
	}
	
	public double[] checkConstraints(int idx, double[] distance){
		ArrayList<Constraint> constraints = photos[idx].getConstraints();
		for(int i=0; i<constraints.size(); i++){
			Constraint c = constraints.get(i);
			//Log.d("Checking Constraint", "Photo with idx " + idx + " " + c.toString());
			int p2 = photoIds.get(c.getP2());
			if(photoInCluster[p2]!=-1){
				distance[photoInCluster[p2]]+=enforceConstraint(c);
				//distance[idx]+=enforceConstraint(c);
				//Log.d("Check Constraints", String.format("Photo with idx %d, new distance to cluster %d = %f",
					//		idx, photoInCluster[photoIds.get(c.getP2())], distance[photoInCluster[p2]]));
			}
		}
		return distance;
	}
	
	//TODO: Sophisticated measure of similarity
	public double enforceConstraint(Constraint c){
		return (c.getSimilarity()-2.5)/2;
	}
	
	public int getMax(double[] distance){
		double max = -1;
		int maxIdx = 0;
		
		/*
		 * If only one cluster is not full, assign the element to it.
		 */
		boolean clusterNotFull = false;
		for(int i=0; i<k; i++) clusterNotFull |= clusters[i].getSize() < maxClusterSize;
		
		for(int i=1; i<k; i++){
			if(clusterNotFull){
				if(distance[i]>max && clusters[i].getSize()< maxClusterSize){
					max = distance[i];
					maxIdx = i;
				}
			} else{
				if(distance[i]>max){
					max = distance[i];
					maxIdx = i;
				}			
			}
		}
		return maxIdx;
	}
	
	public void assign(int photo, int cluster){
		photoInCluster[photo] = cluster;
		clusters[cluster].addPhoto(photos[photo]);
	}
	
	public void recomputeCentroids(){
		for(int i=0; i<k; i++) clusters[i].recomputeCentroid();
	}
}
