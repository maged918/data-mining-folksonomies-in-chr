package tagit.tagit.clustering;

import java.util.*;

import android.graphics.Bitmap;

public class Photo {
	private int id;
	private String title, url;
	private HashMap<String, Integer> maxTags;
	private Bitmap b;
	
	private int[] allTags;
	private ArrayList<Constraint> similarities;
	
	public Photo(int id, String title, String url){
		this.id = id;
		this.title = title;
		this.url = url;
		this.similarities = new ArrayList<Constraint>();
		this.maxTags = new HashMap<String, Integer>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setBitmap(Bitmap b){
		this.b = b;
	}
	
	public Bitmap getBitmap(){
		return b;
	}

	public HashMap<String, Integer> getMaxTags() {
		return maxTags;
	}

	public void setMaxTags(HashMap<String, Integer> tags) {
		this.maxTags = tags;
	}
	
	public int[] getAllTags(){
		return allTags;
	}
	
	public void setAllTags(int[] tagIds){
		this.allTags = tagIds;
	}
	
	public int getTagCount(int i){
		return allTags[i];
	}
	
	public void setTagCount(int idx, int val){
		allTags[idx] = val;
	}
	
	public void addTag(String tag){
		if(maxTags.containsKey(tag))
			maxTags.put(tag, maxTags.get(tag)+1);
		else
			maxTags.put(tag,1);
	}
	
	public ArrayList<Constraint> getConstraints(){
		return similarities;
	}
	
	public void addConstraint(Constraint c){
		similarities.add(c);
	}
	
	public String toString(){
		String s = String.format("Photo id = %s, title = %s.\n", id, title);
		if(maxTags.size()==0) s += "Photo has no tags.\n";
		else s+= "Photo has the following tags: " + maxTags.toString()+"\n";
		return s;
	}
	
}
