package tagit.tagit.clustering;


public class Constraint {
	
	private int p1, p2;
	private double similarity;
	
	public Constraint(int p1, int p2, double similarity){
		this.p1 = p1;
		this.p2 = p2;
		this.similarity = similarity;
	}
	
	public int getP1(){
		return p1;
	}
	
	public int getP2(){
		return p2;
	}
	
	public double getSimilarity(){
		return similarity;
	}
	
	public String toString(){
		return String.format("Constraint between %d and %d with rating = %f", p1, p2, similarity);
	}
	
}
