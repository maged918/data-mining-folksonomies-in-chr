package tagit.tagit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class InitiateClustering extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initiate_clustering);
	}
	
	public void startClustering(View v){
		int k = Integer.parseInt(((EditText)findViewById(R.id.editTextCountClusters)).getText().toString());
		int mode = ((Spinner)findViewById(R.id.spinnerClusteringModes)).getSelectedItemPosition();
		Log.d("Clustering Mode", mode+"");
		Intent intent = new Intent(InitiateClustering.this, StartClustering.class);
		intent.putExtra("k", k);
		intent.putExtra("mode",mode);
		startActivity(intent);
	}
	
}
