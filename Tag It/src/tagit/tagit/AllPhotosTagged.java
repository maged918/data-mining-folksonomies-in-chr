package tagit.tagit;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;

import tagit.tagit.clustering.Photo;
import tagit.tagit.util.DownloadImage;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.DownloadPhotos;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.ImageStorage;
import tagit.tagit.util.JSONParser;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class AllPhotosTagged extends Activity {
	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	public ArrayList<ImageItem> imageItems;
	public String httpResponse;
	public JSONArray jsonArr;
	public Photo[] photos;
	public Context context;
	public int tagId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_photos);
		context = this;
		Intent intent = getIntent();
		tagId = intent.getIntExtra("tagId", 0);
		gridView = (GridView) findViewById(R.id.gridViewAllPhotos);
		String address = getString(R.string.base_address) + "all_photos_tagged.php?tag_id="+tagId;
		DownloadPhotos d = new DownloadPhotos(address, this, photos, imageItems, gridView);
		d.run();
	}
	
}
