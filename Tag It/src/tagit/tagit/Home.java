package tagit.tagit;

import java.util.ArrayList;
import java.util.HashMap;

import tagit.tagit.login.ChangePassword;
import tagit.tagit.login.DatabaseHandler;
import tagit.tagit.login.Login;
import tagit.tagit.login.UserFunctions;
import tagit.tagit.photos.ImageListActivity;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

public class Home extends Activity {

	static String userId;
	public static String baseAddress;
	public static String photoExtension;
	public static int experimentId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        /**
         * Hashmap to load data from the Sqlite database
         **/
         HashMap user = new HashMap();
         user = db.getUserDetails();
         
         //TODO: Display User Name
         userId = (String)user.get(DatabaseHandler.KEY_UID);
         Log.d("User ID", "User ID = " + userId);
         
         baseAddress = getString(R.string.base_address);
         photoExtension = getString(R.string.photo_extension);
         experimentId = Integer.parseInt(getString(R.string.experiment_id));
	}
	
	public void changePassword(View v){
        Intent chgpass = new Intent(getApplicationContext(), ChangePassword.class);
        startActivity(chgpass);
	}
	
	public void logOut(View v){
        UserFunctions logout = new UserFunctions();
        logout.logoutUser(getApplicationContext());
        Intent login = new Intent(getApplicationContext(), Login.class);
        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
        finish();
	}

	public void goToAllPhotos(View v){
		Intent intent = new Intent(Home.this, AllPhotos.class);
		startActivity(intent);
	}
	
	public void goToAllTags(View v){
		Intent intent = new Intent(Home.this, AllTags.class);
		startActivity(intent);
	}
	
	public void goToRateSimilarity(View v){
		Intent intent = new Intent(Home.this, RateSimilarity.class);
		startActivity(intent);
	}
	
	public void goToClustering(View v){
		Intent intent = new Intent(Home.this, InitiateClustering.class);
		startActivity(intent);
	}
	
	public void addPhotos(View v){
		Intent intent = new Intent(Home.this, AddPhotos.class);
		//Intent intent = new Intent(Home.this, ImageListActivity.class);
		startActivity(intent);
	}
	
}
