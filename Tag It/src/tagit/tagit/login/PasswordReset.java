package tagit.tagit.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.R;
import tagit.tagit.util.NetCheck;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class PasswordReset extends Activity {

	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	
	EditText email;
	TextView alert;
	Button resetpass;

    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
  
        setContentView(R.layout.activity_passwordreset);

        Button login = (Button) findViewById(R.id.bktolog);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Login.class);
                startActivityForResult(myIntent, 0);
                finish();
            }
        });

        email = (EditText) findViewById(R.id.forpas);
        alert = (TextView) findViewById(R.id.alert);
        resetpass = (Button) findViewById(R.id.respass);
        resetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetAsync(view);
            }
        });
    }

	public void NetAsync(View view){
	    NetCheck task = new NetCheck(){
			public void receiveData(Boolean b) {
                if(b == true){
                    //nDialog.dismiss();
                    new ProcessRegister().execute();
                }
                else{
                    //nDialog.dismiss();
                    alert.setText("Error in Network Connection");
                }
			}
	    };
	    task.execute(new Object[]{getSystemService(Context.CONNECTIVITY_SERVICE)});
	}
	

	private class ProcessRegister extends AsyncTask<String, String, JSONObject> {
	
	    private ProgressDialog pDialog;
	
	    String forgotpassword;
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        forgotpassword = email.getText().toString();
	
	        pDialog = new ProgressDialog(PasswordReset.this);
	        pDialog.setTitle("Contacting Servers");
	        pDialog.setMessage("Getting Data ...");
	        pDialog.setIndeterminate(false);
	        pDialog.setCancelable(true);
	        pDialog.show();
	    }
	
	    protected JSONObject doInBackground(String... args) {
	        UserFunctions userFunction = new UserFunctions();
	        JSONObject json = userFunction.forPass(forgotpassword);
	        return json;
	    }

	    protected void onPostExecute(JSONObject json) {
	  /**
	   * Checks if the Password Change Process is sucesss
	   **/
	    try {
	        if (json.getString(KEY_SUCCESS) != null) {
	            alert.setText("");
	            String res = json.getString(KEY_SUCCESS);
	            String red = json.getString(KEY_ERROR);

	            if(Integer.parseInt(res) == 1){
	               pDialog.dismiss();
	                alert.setText("A recovery email is sent to you, see it for more details.");
	            }
	            else if (Integer.parseInt(red) == 2)
	            {    pDialog.dismiss();
	                alert.setText("Your email does not exist in our database.");
	            }
	            else {
	                pDialog.dismiss();
	                alert.setText("Error occured in changing Password");
	            }
	        }}
		    catch (JSONException e) {
		        e.printStackTrace();
		    }
	    }	
	}
}





