package tagit.tagit.login;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.Home;
import tagit.tagit.R;
import tagit.tagit.RateSimilarity;
import tagit.tagit.R.id;
import tagit.tagit.R.layout;
import tagit.tagit.util.NetCheck;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class Login extends Activity {
	Button btnLogin;
	Button Btnregister;
	Button passreset;
	EditText inputEmail;
	EditText inputPassword;
	private TextView loginErrorMsg;
	/**
	 * Called when the activity is first created.
	 */
	private static String KEY_SUCCESS = "success";
	private static String KEY_UID = DatabaseHandler.KEY_UID;
	private static String KEY_USERNAME = DatabaseHandler.KEY_USERNAME;
	private static String KEY_FIRSTNAME = DatabaseHandler.KEY_FIRSTNAME;
	private static String KEY_LASTNAME = DatabaseHandler.KEY_LASTNAME;
	private static String KEY_EMAIL = DatabaseHandler.KEY_EMAIL;
	private static String KEY_CREATED_AT = DatabaseHandler.KEY_CREATED_AT;
	private static String KEY_ID = DatabaseHandler.KEY_ID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
		HashMap user = new HashMap();
		user = db.getUserDetails();

		if (user.containsKey("uid")) {
			Intent intent = new Intent(Login.this, Home.class);
			startActivity(intent);
		}
		inputEmail = (EditText) findViewById(R.id.email);
		inputPassword = (EditText) findViewById(R.id.pword);
		Btnregister = (Button) findViewById(R.id.registerbtn);
		btnLogin = (Button) findViewById(R.id.login);
		passreset = (Button) findViewById(R.id.passres);
		loginErrorMsg = (TextView) findViewById(R.id.loginErrorMsg);
		passreset.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(),
						PasswordReset.class);
				startActivityForResult(myIntent, 0);
				finish();
			}
		});
		Btnregister.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(), Register.class);
				startActivityForResult(myIntent, 0);
				finish();
			}
		});
		/**
		 * Login button click event A Toast is set to alert when the Email and
		 * Password field is empty
		 **/
		btnLogin.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				if ((!inputEmail.getText().toString().equals(""))
						&& (!inputPassword.getText().toString().equals(""))) {
					NetAsync(view);
				} else if ((!inputEmail.getText().toString().equals(""))) {
					Toast.makeText(getApplicationContext(),
							"Password field empty", Toast.LENGTH_SHORT).show();
				} else if ((!inputPassword.getText().toString().equals(""))) {
					Toast.makeText(getApplicationContext(),
							"Email field empty", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(),
							"Email and Password field are empty",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void NetAsync(View view) {
		NetCheck task = new NetCheck() {
			public void receiveData(Boolean b) {
				if (b == true) {
					// nDialog.dismiss();
					new ProcessLogin().execute();
				} else {
					// nDialog.dismiss();
					Toast.makeText(Login.this, "Error in Network Connection",
							1000).show();
				}
			}
		};
		task.execute(new Object[] { getSystemService(Context.CONNECTIVITY_SERVICE) });
	}

	/**
	 * Async Task to get and send data to My Sql database through JSON respone.
	 **/
	private class ProcessLogin extends AsyncTask<String, Void, JSONObject> {
		private ProgressDialog pDialog;
		String email, password;

		protected void onPreExecute() {
			super.onPreExecute();
			inputEmail = (EditText) findViewById(R.id.email);
			inputPassword = (EditText) findViewById(R.id.pword);
			email = inputEmail.getText().toString();
			password = inputPassword.getText().toString();
			pDialog = new ProgressDialog(Login.this);
			pDialog.setTitle("Contacting Servers");
			pDialog.setMessage("Logging in ...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected JSONObject doInBackground(String... args) {
			UserFunctions userFunction = new UserFunctions();
			JSONObject json = userFunction.loginUser(email, password);
			return json;
		}

		protected void onPostExecute(JSONObject json) {
			try {
				if (json.getString(KEY_SUCCESS) != null) {
					String res = json.getString(KEY_SUCCESS);
					if (Integer.parseInt(res) == 1) {
						pDialog.setMessage("Loading User Space");
						pDialog.setTitle("Getting Data");
						DatabaseHandler db = new DatabaseHandler(
								getApplicationContext());
						JSONObject json_user = json.getJSONObject("user");
						// Log.d("Login Activity - On Post Execute",
						// json_user.toString());
						/**
						 * Clear all previous data in SQlite database.
						 **/
						UserFunctions logout = new UserFunctions();
						logout.logoutUser(getApplicationContext());
						db.addUser(json_user.getString(KEY_FIRSTNAME),
								json_user.getString(KEY_LASTNAME),
								json_user.getString(KEY_EMAIL),
								json_user.getString(KEY_USERNAME),
								json_user.getString(KEY_UID),
								json_user.getString(KEY_CREATED_AT));
						/**
						 * If JSON array details are stored in SQlite it
						 * launches the User Panel.
						 **/
						Intent upanel = new Intent(getApplicationContext(),
								Home.class);
						upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						pDialog.dismiss();
						startActivity(upanel);
						/**
						 * Close Login Screen
						 **/
						finish();
					} else {
						pDialog.dismiss();
						loginErrorMsg.setText("Incorrect username/password");
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}
