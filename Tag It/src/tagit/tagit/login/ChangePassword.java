package tagit.tagit.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.Home;
import tagit.tagit.R;
import tagit.tagit.util.NetCheck;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
public class ChangePassword extends Activity {
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";
    EditText newpass;
    TextView alert;
    Button changepass;
    Button cancel;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        cancel = (Button) findViewById(R.id.btcancel);
        cancel.setOnClickListener(new View.OnClickListener(){
        public void onClick(View arg0){
                Intent login = new Intent(getApplicationContext(), Home.class);
                startActivity(login);
                finish();
            }
        });
        newpass = (EditText) findViewById(R.id.newpass);
        alert = (TextView) findViewById(R.id.alertpass);
        changepass = (Button) findViewById(R.id.btchangepass);
        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetAsync(view);
            }
        });}
    
    public void NetAsync(View view){
        NetCheck task = new NetCheck(){
        	public void receiveData(Boolean b){
                if(b == true){
                    //nDialog.dismiss();
                    new ProcessRegister().execute();
                }
                else{
                    //nDialog.dismiss();
                    alert.setText("Error in Network Connection");
                }
        	}
        };
        task.execute(new Object[]{getSystemService(Context.CONNECTIVITY_SERVICE)});
    }
 
    private class ProcessRegister extends AsyncTask<String, Void, JSONObject> {
        private ProgressDialog pDialog;
        String newpas,email;
  
        protected void onPreExecute() {
            super.onPreExecute();
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            HashMap user = new HashMap();
            user = db.getUserDetails();
            newpas = newpass.getText().toString();
            email = (String)user.get("email");
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setTitle("Contacting Servers");
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
 
        protected JSONObject doInBackground(String... args) {
            UserFunctions userFunction = new UserFunctions();
            JSONObject json = userFunction.chgPass(newpas, email);
            Log.d("Button", "Register");
            return json;
        }
        
        protected void onPostExecute(JSONObject json) {
            try {
                if (json.getString(KEY_SUCCESS) != null) {
                    alert.setText("");
                    String res = json.getString(KEY_SUCCESS);
                    String red = json.getString(KEY_ERROR);
                    if (Integer.parseInt(res) == 1) {
                        /**
                         * Dismiss the process dialog
                         **/
                        pDialog.dismiss();
                        alert.setText("Your Password is successfully changed.");
                    } else if (Integer.parseInt(red) == 2) {
                        pDialog.dismiss();
                        alert.setText("Invalid old Password.");
                    } else {
                        pDialog.dismiss();
                        alert.setText("Error occured in changing Password.");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
