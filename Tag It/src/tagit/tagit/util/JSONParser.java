package tagit.tagit.util;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.Home;
import tagit.tagit.R;
import tagit.tagit.clustering.Constraint;
import tagit.tagit.clustering.Photo;
import android.util.Log;

public class JSONParser {
	public static Photo[] loadPhotosJSON(String httpResponse){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			Photo[] photos = new Photo[jsonArr.length()+1];
			//Log.d("JSON Photo Parser", photos.length+"");
			for(int i=0; i<jsonArr.length(); i++){
				JSONObject obj = jsonArr.getJSONObject(i);
				String url = Home.baseAddress+ obj.getString("url")
						+"img"+obj.getString("photo_id")+Home.photoExtension;
				String title = obj.getString("title");
				String id = obj.getString("photo_id");
				photos[i] = new Photo(Integer.parseInt(id), title, url);
				//Log.d("JSON Parser", "Photo #"+ photos[i].getUrl());
			}
			photos[jsonArr.length()] = new Photo(jsonArr.length(), "", "");
			return photos;

		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
		return null;
	}
	
	public static Photo[] loadSimilarityJSON(String httpResponse){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			Photo[] photos = new Photo[2];
			for(int i=1; i<=2; i++){
				JSONObject obj = jsonArr.getJSONObject(0);
				String url = Home.baseAddress 
						+ obj.getString("url_"+i)+"img"+obj.getString("photo_id_"+i)+ Home.photoExtension;
				String title = obj.getString("title_"+i);
				String id = obj.getString("photo_id_"+i);
				photos[i-1] = (new Photo(Integer.parseInt(id), title, url));
				//Log.d("Similiarity JSON", "Tag #" + obj.getString("tag_"+i));
			}
			return photos;
		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
		return null;
	}
	
	public static int loadMaxTagJSON(String httpResponse){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			JSONObject obj = jsonArr.getJSONObject(0);
			return Integer.parseInt(obj.getString("max_tag"));
		} catch(JSONException e){
			//e.printStackTrace();
			Log.e("JSON Parser Max Tag", "Error loading JSON " + e.toString());
		}
		return -1;
	}
	
	/*public static HashMap<Integer, Integer> loadPhotoTags(String httpResponse){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			HashMap<Integer, Integer> tagMap = new HashMap<Integer, Integer>();
			//Log.d("JSON Photo Parser", photos.length+"");
			for(int i=0; i<jsonArr.length(); i++){
				JSONObject obj = jsonArr.getJSONObject(i);
				tagMap.put(Integer.parseInt(obj.getString("tag_id")), Integer.parseInt(obj.getString("count")));
				//Log.d("JSON Parser", "Photo #"+ photos[i].getUrl());
			}
			return tagMap;
		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
		return new HashMap<Integer, Integer>();
	}*/
	
	public static HashMap[] loadPhotosTags(String httpResponse, int countPhotos){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			HashMap<Integer, Integer>[] tagMap = new HashMap[countPhotos+1];
			for(int i=1; i<=countPhotos; i++) tagMap[i] = new HashMap<Integer, Integer>();
			//Log.d("JSON Photo Parser", photos.length+"");
			for(int i=0; i<jsonArr.length(); i++){
				JSONObject obj = jsonArr.getJSONObject(i);
				int photo_id = Integer.parseInt(obj.getString("photo_id"));
				if(obj.getString("tag_id")!=null && !obj.getString("tag_id").equals("null")){
					tagMap[photo_id].put(Integer.parseInt(obj.getString("tag_id")), 
							Integer.parseInt(obj.getString("count")));
				}
							
				//Log.d("JSON Parser", "Photo #"+ photos[i].getUrl());
			}
			return tagMap;
		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
		return new HashMap[0];
	}
	
	public static Constraint[] loadConstraints(String httpResponse){
		try{
			JSONArray jsonArr = new JSONArray(httpResponse);
			Constraint[] arr = new Constraint[jsonArr.length()];
			for(int i=0; i<jsonArr.length(); i++){
				JSONObject obj = jsonArr.getJSONObject(i);
				int p1 = Integer.parseInt(obj.getString("photo_1"));
				int p2 = Integer.parseInt(obj.getString("photo_2"));
				double rating = Double.parseDouble(obj.getString("rating"));
				arr[i] = new Constraint(p1, p2, rating);
			}
			return arr;
		} catch (JSONException e){
			Log.e("Load JSON Constraints", "Error loading JSON " + e.toString());
		}
		return null;
	}
}
