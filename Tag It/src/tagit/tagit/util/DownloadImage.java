package tagit.tagit.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

	
	
interface DowloaderInterface{
	public void receiveData(Object o);
}
	
public abstract class DownloadImage extends AsyncTask<URL, Void, Object> implements DowloaderInterface{
	
	public abstract void receiveData(Object o);
	
	protected Object doInBackground(URL... params) {
	    Bitmap bmImg = null;
	    try {
	    	HttpURLConnection conn= (HttpURLConnection)params[0].openConnection();
	    	conn.setDoInput(true);
	    	conn.connect();
	    	InputStream is = conn.getInputStream();

	    	bmImg = BitmapFactory.decodeStream(is);
	    } catch (IOException e) {
	    	// TODO Auto-generated catch block
	    	e.printStackTrace();
	    	return null;
	    }
    	return bmImg;
	}
	
	protected void onPostExecute(Object o){
		receiveData(o);
	}
}

