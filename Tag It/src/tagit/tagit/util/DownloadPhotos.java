package tagit.tagit.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import tagit.tagit.AllPhotos;
import tagit.tagit.R;
import tagit.tagit.RateSimilarity;
import tagit.tagit.ViewPhoto;
import tagit.tagit.clustering.Photo;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class DownloadPhotos {
	
	public static final int RATE_SIMILARITY_MODE = 1;
	
	String address;
	Context context;
	ProgressDialog pDialog;
	Photo[] photos;
	ArrayList<ImageItem> imageItems;
	GridView gridView;
	GridViewAdapter customGridAdapter;
	
	int mode = 0; // similarity flag to indicate if this is the Rate Similarity activity
	
	public DownloadPhotos(String address, Context context, Photo[] photos, ArrayList<ImageItem> imageItems,
			GridView gridView, int mode){
		this(address, context, photos, imageItems, gridView);
		this.mode = mode;
	}
	
	public DownloadPhotos(String address, Context context, Photo[] photos, ArrayList<ImageItem> imageItems,
			GridView gridView){
		this(context, photos, imageItems, gridView);
		this.address = address;
	}
	
	public DownloadPhotos(Context context, Photo[] photos, ArrayList<ImageItem> imageItems,
			GridView gridView){
		this.address = address;
		this.context = context;
		this.photos = photos;
		this.imageItems = imageItems;
		this.gridView = gridView;
	}
	
	public void run(){
		setProgressDialog(1);
		downloadPhotosJSON();
	}
	
	public void download(){
		setProgressDialog(1);
		downloadPhotos();
	}
	
	public void setProgressDialog(int mode){
		pDialog = new ProgressDialog(context);
        pDialog.setTitle("Contacting Servers");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
		switch(mode){
		case 1:
            pDialog.setMessage("Downloading Photo Information..");
            break;
		case 2:
            pDialog.setMessage("Downloading Photos..");
            break;
		}
	}
	
	public void downloadPhotosJSON(){
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				//httpResponse = s;
				switch(mode){
				case RATE_SIMILARITY_MODE: photos = JSONParser.loadSimilarityJSON(s);
					RateSimilarity.setPhotos(photos);
					Log.d("Download Photos", photos[0].toString());
					break;
				default: photos = JSONParser.loadPhotosJSON(s);
					break;
				}
				pDialog.dismiss();
				downloadPhotos();
			}
		};
		task.execute(new String[]{address});
	}
	
	public void downloadPhotos(){
		//remainingPhotos = photos.length;
		imageItems = new ArrayList<ImageItem>();
		for(int i=0; i<photos.length; i++){
			try{
				//Log.d("Download Photos", photos[i].toString());
				downloadImage(i);
			} catch (IOException e){
				Log.e("Download Photos", e.toString());
			}
		}
		
		//Log.d("Download Photos", "Size of photos array = " + imageItems.size());
	}
	
	public void downloadImage(final int i) throws MalformedURLException{
		if(i==photos.length-1) pDialog.dismiss();
		
		//TODO: Fix this hack!
		if(photos[i]==null) return;
		
		final int photoId = photos[i].getId();
		//Log.d("Remaining Photos", remainingPhotos+"");
        //if(remainingPhotos==1) pDialog.dismiss();
		
        if(ImageStorage.checkifImageExists("img"+photoId))
        {
			File file = ImageStorage.getImage("/img"+photoId+".jpg"); 
            String path = file.getAbsolutePath(); 
            if (path != null){
                Bitmap b = BitmapFactory.decodeFile(path);
				photos[i].setBitmap(b);
				imageItems.add(Math.min(i,imageItems.size()),new ImageItem(b, photos[i].getTitle()));
				populatePhotos();
            }  
        } else { 
        	DownloadImage task = new DownloadImage(){
    			public void receiveData(Object o){
    				Bitmap b = (Bitmap) o;
    				imageItems.add(Math.min(i,imageItems.size()),new ImageItem(b, photos[i].getTitle()));
    				photos[i].setBitmap(b);
    				populatePhotos();
    				ImageStorage.saveToSdCard(b, "img"+photoId);
    			}
    		};
    		task.execute(new URL(photos[i].getUrl()));
        }
		
	}
	
	public void populatePhotos(){
		
		customGridAdapter = new GridViewAdapter(context, R.layout.row_grid,
				imageItems);
		gridView.setAdapter(customGridAdapter);
		OnItemClickListener o = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				Intent intent = new Intent(context, ViewPhoto.class);
				intent.putExtra("url", photos[position].getUrl());
				intent.putExtra("id", photos[position].getId()+"");
				intent.putExtra("title", photos[position].getTitle());
				context.startActivity(intent);
			}
		};
		gridView.setOnItemClickListener(o);		

	}
}
