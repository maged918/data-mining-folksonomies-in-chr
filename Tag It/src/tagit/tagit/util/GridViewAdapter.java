package tagit.tagit.util;

import java.util.ArrayList;

import tagit.tagit.R;
import tagit.tagit.R.id;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
/**
 *
 * @author javatechig {@link http://javatechig.com}
 *
 */
public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList data = new ArrayList();
 
    public GridViewAdapter(Context context, int layoutResourceId,
            ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
 
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
 
        ImageItem item = (ImageItem) data.get(position);
        holder.image.setImageBitmap(item.getImage());
        //holder.image.setLayoutParams(new GridView.LayoutParams(80, 80));
        //holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return row;
    }
 
    static class ViewHolder {
        ImageView image;
    }
}