package tagit.tagit.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

interface DownloadJSONInterface{
	public void receiveData(String s);
}

public abstract class DownloadJSON extends AsyncTask <String, Void, String> implements DownloadJSONInterface{

	@Override
	protected String doInBackground(String... urls) {
		//Log.d("Do In Background", "Here!");
		try {
			InetAddress address = InetAddress.getByName(urls[0]);
		} catch (UnknownHostException e) {
			//Log.d("DNS Error", e.getMessage());
		}
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost post = new HttpPost(urls[0]);
		try{
			HttpResponse response = httpClient.execute(post);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = "";
			StringBuffer sb = new StringBuffer();
			while((line=br.readLine())!=null){
				sb.append(line + "\n");
				Log.d("HTTP Response ", line);
			}
			is.close();
			
			String s = sb.toString();

			return s;
		} catch(IOException e){
			e.printStackTrace();
			Log.d("queryAllTags", "Error from http response " + e.toString());
		}
		return null;
	}
	
	public void onPostExecute(String s){
		receiveData(s);
	}

}