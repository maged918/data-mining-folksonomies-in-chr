package tagit.tagit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.clustering.Photo;
import tagit.tagit.login.Login;
import tagit.tagit.util.DownloadImage;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.ImageStorage;
import tagit.tagit.util.NetCheck;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ViewPhoto extends Activity {
	
	static Photo p;
	private ArrayList imageItems;
	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	private HashMap<String, Integer> tags;
	private JSONArray jsonArr;
	private String httpResponse;
	private TextView tagCloud;
	private static ViewPhoto self;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_photo);
		tagCloud= (TextView)findViewById(R.id.tagCloud);
		self = this;
		Intent i = getIntent();
		p = new Photo(Integer.parseInt(i.getStringExtra("id")), 
				i.getStringExtra("title"), i.getStringExtra("url"));
		NetAsync();
	}
	
    public void NetAsync(){
        NetCheck task = new NetCheck(){
			public void receiveData(Boolean b) {
				if(b == true){
	                //nDialog.dismiss();
					try {
						downloadImage();
						downloadTags();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
	            }
	            else{
	                //nDialog.dismiss();
	            	Toast.makeText(ViewPhoto.this, "Error in Network Connection", 1000).show();
	            }
			}
        };
        task.execute(new Object[]{getSystemService(Context.CONNECTIVITY_SERVICE)});
    }
	
	public void downloadImage() throws MalformedURLException{
        final int photoId = p.getId();
        imageItems = new ArrayList();
		//Log.d("View Photo", "ID = " + photoId);
		if(ImageStorage.checkifImageExists("img"+photoId))
        {
            File file = ImageStorage.getImage("/img"+photoId+".jpg"); 
            String path = file.getAbsolutePath();
    		//Log.d("View Photo", "URL = " + path);
            if (path != null){
                Bitmap b = BitmapFactory.decodeFile(path);
				//int idx = (int) (Math.random() * imageItems.size());
				imageItems.add(new ImageItem(b, p.getId()+""));
				populatePhoto();
            }  
        } else { 
        	DownloadImage task = new DownloadImage(){
    			public void receiveData(Object o){
    				Bitmap b = (Bitmap) o;
    				//Log.d("Download Image", photos[i].getTitle());
    				//int idx = (int) (Math.random() * imageItems.size());
    				imageItems.add(new ImageItem(b, p.getId()+""));
    				ImageStorage.saveToSdCard(b, "img"+photoId);
    				populatePhoto();
    			}
    		};
    		task.execute(new URL(p.getUrl()));
        }
		
	}
	
	
	public void populatePhoto(){
		gridView = (GridView) findViewById(R.id.gridViewSinglePhoto);
		customGridAdapter = new GridViewAdapter(this, R.layout.row_grid,
				imageItems);
		gridView.setAdapter(customGridAdapter);
		OnItemClickListener o = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
			}
		};
		gridView.setOnItemClickListener(o);		
	}
	
	public void downloadTags(){
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				if(s!=null && !s.equals("null\n")){
					httpResponse = s;
					loadJSON();
				}
			}
		};
		task.execute(new String[]{getString(R.string.base_address)
					+"max_photo_tags.php?photo_id=" + p.getId()});
	}
	
	public void loadJSON(){
		try{
			jsonArr = new JSONArray(httpResponse);
			tags = new HashMap<String, Integer>();
			if(jsonArr.length()!=0) tagCloud.setText("Top Tags: ");
			for(int i=0; i<jsonArr.length(); i++){
				JSONObject obj = jsonArr.getJSONObject(i);
				String tag = obj.getString("tag");
				int count = Integer.parseInt(obj.getString("count"));
				//Log.d("Photo Tags", tag + " " + count);
				tags.put(tag, count);
				tagCloud.setText(tagCloud.getText()+" #" + tag);
			}
			p.setMaxTags(tags);
		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
	}
	
	public void addTag(View v){
		
		String tag = ((EditText)findViewById(R.id.addTagText)).getText().toString();
		if(tag.length()==0) {
			Toast.makeText(this, "Please enter a tag", 1000).show();
			return;
		}
		if(tag.charAt(0)=='#') tag = tag.substring(1);
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				if(s.equals("1\n")){
					runOnUiThread(new Runnable() {
						public void run() {
						    Toast.makeText(ViewPhoto.this, "Tag Added!", Toast.LENGTH_SHORT).show();
						    }
					});
				}
			}
		};
		
		List<NameValuePair> params = new LinkedList<NameValuePair>();
		params.add(new BasicNameValuePair("user_id", Home.userId));
		params.add(new BasicNameValuePair("photo_id", p.getId()+""));
		params.add(new BasicNameValuePair("tag", tag));
		String s = URLEncodedUtils.format(params, "utf-8");
		task.execute(new String[]{getString(R.string.base_address)+
				"save_tag.php?" + s});

		if(tags!=null && !tags.containsKey(tag)){
			tagCloud.setText(tagCloud.getText() + " #"+tag);
		} else if(tags==null){
			tags = new HashMap<String, Integer>();
			tagCloud.setText(tagCloud.getText() + "#"+tag);
		}
		((EditText)findViewById(R.id.addTagText)).setText("");
	}
	
	
}
