package tagit.tagit;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public class AllTags extends Activity {
	
	private String[] tags;
	private int[] tagIds;
	private String httpResponse;
	private ListView mainListView;
	private ArrayAdapter<String> listAdapter ; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_tags);
		//tags = new String[]{"some", "random", "tags"};
		DownloadAllTags task = new DownloadAllTags();
		task.execute(new String[]{getString(R.string.base_address) + "all_tags.php"});
		
		// Find the ListView resource.   
	    mainListView = (ListView) findViewById( R.id.allTagsList );  
	  
	    // Create ArrayAdapter using an empty list.  
	    listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, new ArrayList<String>());  
	     	      
	    //Set the ArrayAdapter as the ListView's adapter.  
	    mainListView.setAdapter( listAdapter );
	    
	    mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                    long id) {
                Intent intent = new Intent(AllTags.this, AllPhotosTagged.class);
                int tagId = tagIds[position];
                intent.putExtra("tagId", tagId);
                startActivity(intent);
            }
        });

	}
	
	public void loadJSON(){
		try{
			JSONArray arr = new JSONArray(httpResponse);
			tags = new String[arr.length()];
			tagIds = new int[arr.length()];
			for(int i=0; i<arr.length(); i++){
				JSONObject obj = arr.getJSONObject(i);
				tags[i] = obj.getString("tag");
				tagIds[i] = Integer.parseInt(obj.getString("tag_id"));
			}
		} catch(JSONException e){
			//e.printStackTrace();
			Log.d("loadJSON", "Error loading JSON " + e.toString());
		}
	}
	
	public void populateTags(){
		for(String tag : tags){
			/*TextView tv = new TextView(this);
			tv.setText(tag);
			tv.setTextSize(20);
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f));
			tv.setGravity(Gravity.LEFT | Gravity.CENTER);
			g.addView(tv);*/
			listAdapter.add(tag);
		}
	}
	
	private class DownloadAllTags extends AsyncTask <String, Void, String>{

		@Override
		protected String doInBackground(String... urls) {
			Log.d("Do In Background", "Here!");
			try {
				InetAddress address = InetAddress.getByName(urls[0]);
			} catch (UnknownHostException e) {
				Log.d("DNS Error", e.getMessage());
			}
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(urls[0]);
			try{
				HttpResponse response = httpClient.execute(post);
				HttpEntity entity = response.getEntity();
				InputStream is = entity.getContent();
				
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				String line = "";
				StringBuffer sb = new StringBuffer();
				while((line=br.readLine())!=null){
					sb.append(line + "\n");
					Log.d("HTTP Response ", line);
				}
				is.close();
				
				String s = sb.toString();
				return s;
			} catch(IOException e){
				e.printStackTrace();
				Log.d("queryAllTags", "Error from http response " + e.toString());
			}
			return null;
		}
		
		protected void onPostExecute(String result){
			//Log.d("On Post Execute", result);
			httpResponse = result;
			loadJSON();
			populateTags();
		}
	}
	
}


