package tagit.tagit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import tagit.tagit.util.DownloadJSON;
import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class AddPhotos extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_photos);
		
		//addPhotos();
		addTags();
		
	}
	
	public void addPhotos(){
		for(int i=155; i<=380; i++){
			DownloadJSON task = new DownloadJSON(){
				public void receiveData(String s){
					
				}
			};
			task.execute(new String[]{getString(R.string.base_address)+"add_photo.php?photo_id="+i});
		}
	}

	public void addTags(){
		try{
			AssetManager am = this.getAssets();
			BufferedReader br = new BufferedReader(new InputStreamReader(am.open("Tags.txt")));
			for(int i=1; i<=78; i++) br.readLine();
			for(int i=79; i<=380; i++){
				StringTokenizer st = new StringTokenizer(br.readLine(),",");
				while(st.hasMoreTokens()){
					DownloadJSON task = new DownloadJSON(){
						public void receiveData(String s){
							
						}
					};
					List<NameValuePair> params = new LinkedList<NameValuePair>();
					params.add(new BasicNameValuePair("user_id", Home.userId));
					params.add(new BasicNameValuePair("photo_id", i+""));
					params.add(new BasicNameValuePair("tag", st.nextToken()));
					String s = URLEncodedUtils.format(params, "utf-8");
					task.execute(new String[]{getString(R.string.base_address)+
							"save_tag.php?" + s});
				}
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
