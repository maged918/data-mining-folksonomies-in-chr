package tagit.tagit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.nullwire.trace.ExceptionHandler;

import tagit.tagit.clustering.Cluster;
import tagit.tagit.clustering.ConstrainedKMeans;
import tagit.tagit.clustering.Constraint;
import tagit.tagit.clustering.Photo;
import tagit.tagit.login.Login;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.JSONParser;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RatingBar;

public class StartClustering extends Activity{

	static JSONArray jsonArr;
	static Photo[] photos;
	static Constraint[] constraints;
	static Cluster[] constrainedClusters;
	static Cluster[] nonConstrainedClusters;
	static ConstrainedKMeans clustering;
	static int k;
	static int mode;
	static int limitItems;
	static int maxId;
	static int remaining;
	static int constrained;
	private GridView gridView;
	private GridViewAdapter customGridAdapter;
	public ArrayList<ImageItem> imageItems;
	static int[] clusterPositions;
	static int clusteringId;
	static Context context;
	static ProgressDialog pDialog;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_clustering);
		
		//ExceptionHandler.register(this, "http://tagit.atwebpages.com/error_log.php"); 
		
		Intent i = getIntent();
		k = i.getIntExtra("k", 2);
		mode = i.getIntExtra("mode", 0);
		limitItems = i.getIntExtra("limitItems", 0);
		constrained = 0;
		context = this;
		setProgressDialog();
		downloadPhotos(mode);
		
		//Process: Download Photos, Get Max Tag ID, Download Photo Tags, Download Constraints
	}
	
	public void setProgressDialog(){
        pDialog = new ProgressDialog(StartClustering.this);
        pDialog.setTitle("Operation in Progress");
        String message = constrained == 0? "First" : "Second";
        pDialog.setMessage(message + " clustering operation in progress.. Please wait a few seconds.");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
	}
	
	public void downloadPhotos(int mode){
		switch(mode){
		case 0: downloadPhotos(getString(R.string.base_address) + "all_photos.php");
		default: return;
		}
	}
	
	public void downloadPhotos(String url){
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				photos = JSONParser.loadPhotosJSON(s);
				getMaxId();				
			}
		};
		task.execute(new String[]{url});
	}
	
	public void getMaxId(){
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String val){
				maxId = JSONParser.loadMaxTagJSON(val);
				Log.d("Max Tag ID", "Max Tag ID = " + maxId);
				initializeTagVectors();
				downloadPhotoTags();
				downloadConstraints();
			}
		};
		task.execute(getString(R.string.base_address) + "max_tag_id.php");
	}
	
	public void initializeTagVectors(){
		for(int i=0; i<photos.length; i++){
			//Log.d("Initializing Tag Vectors", "Max ID = " + maxId);
			photos[i].setAllTags(new int[maxId+1]);
			//Log.d("Initializing Tag Vectors", "Photo #" + photos[i].getId() + " " + 
			//									photos[i].getAllTags().length);
		}
	}
	
	public void downloadPhotoTags(){
		remaining = photos.length;

		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				HashMap<Integer, Integer>[] tagMap= JSONParser.loadPhotosTags(s, photos.length);
				for(int i=0; i<tagMap.length; i++){
					if(tagMap[i]!=null && tagMap[i].size()!=0){
						for(Entry<Integer, Integer> entry: tagMap[i].entrySet()){
							photos[i].setTagCount(entry.getKey(), entry.getValue());
							//Log.d("Photos Tags", String.format("Photo %d, tag%d, count%d", 
								//	i, entry.getKey(), entry.getValue()));
						}
					}
					remaining--;
				}
				//Log.d("Photo tags", "Photo #" + photos[idx].getId() + ": " + 
					//	Arrays.toString(photos[idx].getAllTags()));
			}
		};
		task.execute(getString(R.string.base_address)+"all_photos_tags.php");
	
	}
	
	public void downloadConstraints(){
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				constraints = JSONParser.loadConstraints(s);
				while(remaining>0) continue;
				constrainedClusters = startClustering(0);
				/*for(int i=0; i<constraints.length; i++){
				Log.d("Constraints", "Constraint: " + constraints[i]);
				}*/
			}
		};
		task.execute(getString(R.string.base_address)+"all_constraints.php");
	}
	
	public Cluster[] startClustering(int mode){
		Log.d("Start Clustering", "Right Before Clustering");
		
		clustering = new ConstrainedKMeans();
		if(mode == 0) constrainedClusters = clustering.constrainedClustering(k, photos, constraints);
		else nonConstrainedClusters = clustering.constrainedClustering(k, photos, constraints);
		
		Log.d("Start Clustering", "Right After Clustering");
		pDialog.dismiss();
		
		//for(int i=0; i<clusters.length; i++) Log.d("Clusters", clusters[i].toString());
		
		populateClusters(0);
		
		return constrainedClusters;
	}
	
	public void populateClusters(int mode){
		Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.folder);
		imageItems = new ArrayList();
		clusterPositions = new int[k];
		int pos = 0;
		final Cluster[] clustersToPopulate = mode==0? constrainedClusters : nonConstrainedClusters;
		for(int i=0; i<k; i++){
			if(clustersToPopulate[i].getSize()!=0){
				imageItems.add(new ImageItem(bitmap, "Cluster #" + i));
				clusterPositions[pos++] = i;
			}
		}
		gridView = (GridView) findViewById(R.id.gridViewClusters);
		customGridAdapter = new GridViewAdapter(this, R.layout.row_grid,
				imageItems);
		gridView.setAdapter(customGridAdapter);
		OnItemClickListener o = new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				
				//Toast.makeText(AllPhotos.this, photos.get(position).getId(),1000).show();
				Intent intent = new Intent(StartClustering.this, ViewCluster.class);
				intent.putExtra("ids", clustersToPopulate[clusterPositions[position]].printElementIds());
				intent.putExtra("urls", clustersToPopulate[clusterPositions[position]].printElementUrls());
				intent.putExtra("titles", clustersToPopulate[clusterPositions[position]].printElementTitles());
				intent.putExtra("uncertainty", clustersToPopulate[clusterPositions[position]].printElementUncertainty());
				intent.putExtra("constrained", constrained);
				intent.putExtra("clusterId", clusterPositions[position]);
				startActivity(intent);
			}
		};
		gridView.setOnItemClickListener(o);		

	}
	
	public void evaluateClustering(View v){
		double rating = ((RatingBar)findViewById(R.id.saveClusteringRating)).getRating();
		DownloadJSON task = new DownloadJSON(){
			public void receiveData(String s){
				StringTokenizer st = new StringTokenizer(s);
				clusteringId = Integer.parseInt(st.nextToken());
				//Log.d("Clustering ID", clustering+"");
				
				Toast.makeText(StartClustering.this, 
						String.format("Clustering #%d Evaluated", constrained), 1000).show();
			}
		};
		task.execute(new String[]{
			String.format(getString(R.string.base_address)
			+"save_clustering.php?user_id=%s&mode_id=%d&k=%d&constrained=%d&limit_items=%d&rating=%f&experiment_id=%d",
						Home.userId, mode, k, constrained, limitItems, rating, Home.experimentId)});
		
		//saveClusters(constrained);
		if(constrained==0){
			((Button) v).setText("Evaluate Second Clustering");
			setProgressDialog();
			
			constraints = new Constraint[0];
			startClustering(1);
			populateClusters(1);
			Toast.makeText(this, "Please evaluate second clustering!", Toast.LENGTH_SHORT).show();
			pDialog.dismiss();
		} else{
			Intent i = new Intent(StartClustering.this, Home.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
		}
		constrained++;
	}
	
	public void saveClusters(int mode){
		SaveClusters task = new SaveClusters();
		task.execute(new String[]{mode+"", getString(R.string.base_address)+"save_clusters.php"});
	}
	
	private class SaveClusters extends AsyncTask<String, Void, String>{
		public String doInBackground(String... urls){
			try {
				InetAddress address = InetAddress.getByName(urls[1]);
			} catch (UnknownHostException e) {
				//Log.d("DNS Error", e.getMessage());
			}
			Cluster[] clustersToSave;
			if(urls[0].equals("0")) clustersToSave = constrainedClusters;
			else clustersToSave = nonConstrainedClusters;
			
			for(int i=0; i<clustersToSave.length; i++){
				StringTokenizer st = new StringTokenizer(clustersToSave[i].printElementIds(),",");
				for(int j=0; j<clustersToSave[i].getSize(); j++){
					HttpClient httpClient = new DefaultHttpClient();
					HttpPost post = new HttpPost(
							String.format(urls[1]+"?clustering_id=%d&cluster_id=%d&photo_id=%d",
							clusteringId, i, Integer.parseInt(st.nextToken())))	;
					try{
						HttpResponse response = httpClient.execute(post);
						HttpEntity entity = response.getEntity();
						InputStream is = entity.getContent();
						
						BufferedReader br = new BufferedReader(new InputStreamReader(is));
						String line = "";
						StringBuffer sb = new StringBuffer();
						while((line=br.readLine())!=null){
							sb.append(line + "\n");
							Log.d("HTTP Response ", line);
						}
						is.close();
						
						String s = sb.toString();
	
					} catch(IOException e){
						e.printStackTrace();
						Log.d("Save Clusters", "Error from http response " + e.toString());
					}
				}
			}
			return "1";
		}
		
		
		public void onPostExecute(String s){
			//Log.d("Save Clusters On Post Execute", s);
		}
	}

}
