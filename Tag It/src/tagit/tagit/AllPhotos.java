package tagit.tagit;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tagit.tagit.clustering.Photo;
import tagit.tagit.login.Login;
import tagit.tagit.util.DownloadJSON;
import tagit.tagit.util.DownloadPhotos;
import tagit.tagit.util.GridViewAdapter;
import tagit.tagit.util.ImageItem;
import tagit.tagit.util.DownloadImage;
import tagit.tagit.util.ImageStorage;
import tagit.tagit.util.JSONParser;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class AllPhotos extends Activity {

	private GridView gridView;
	public ArrayList<ImageItem> imageItems;
	public String httpResponse;
	public JSONArray jsonArr;
	public Photo[] photos;
	public Context context;
	public ProgressDialog pDialog;
	public volatile int remainingPhotos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_all_photos);
		context = this;
		gridView = (GridView) findViewById(R.id.gridViewAllPhotos);
		String address = getString(R.string.base_address) + "all_photos_limit.php";
		DownloadPhotos d = new DownloadPhotos(address, this, photos, imageItems, gridView);
		d.run();
	}

}
