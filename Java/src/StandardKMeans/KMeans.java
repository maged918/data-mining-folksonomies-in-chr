package StandardKMeans;
import java.util.*;
import java.io.*;

public class KMeans {
	
		
	public static ArrayList<Point>[] clusterPoints (Point[] points, int numClusters){
		
		ArrayList<Point>[] clusters = new ArrayList[numClusters];
		for(int i=0; i<numClusters; i++) clusters[i] = new ArrayList<Point>();
		
		Point[] centroids = new Point[numClusters];
		for(int i=0; i<numClusters; i++) centroids[i] = points[i]; //TODO: Improve random assignment
		
		boolean centroidsChanged = true;
		
		while(centroidsChanged){
			centroidsChanged = false;
			
			//Empty the clusters
			for(int i=0; i<numClusters; i++) clusters[i] = new ArrayList<Point>();
			
			//Assign to clusters step
			for(int i=0; i<points.length; i++){
				
				//Calculate min distance
				int idx = 0; double minDistance = Double.MAX_VALUE;
				for(int j=0; j<numClusters; j++){
					double curDistance = points[i].calculateDistance(centroids[j]);
					if(curDistance < minDistance){
						idx = j;
						minDistance = curDistance;
					}
				}
				
				//Assign to cluster
				clusters[idx].add(points[i]);
			}
			
			//Reassign centroids step
			for(int i=0; i<numClusters; i++){
				Point newAverage = Point.calculateAverage(clusters[i]);
				if(!newAverage.equals(centroids[i])){
					centroidsChanged = true;
					centroids[i] = newAverage;
				}
			}
			
		}
		
		return clusters;
		
	}
	

	
}
