package StandardKMeans;
import java.util.ArrayList;


public class Point {
	
	static double eps = Math.pow(10, -6);
	double x;
	double y;
	
	public Point(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Point p){
		return Math.abs(p.x - this.x) < eps && Math.abs(p.y - this.y) < eps;
	}
	
	public String toString(){
		return "X = " + x + " & Y = " + y;
	}
	
	public double calculateDistance (Point p){
		return Math.sqrt(Math.pow((p.x-this.x),2)+Math.pow((p.y-this.y),2));
	}
	
	public static Point calculateAverage(ArrayList<Point> points){
		double sumX = 0; double sumY = 0;
		int size = points.size();
		
		for(int i=0; i<size; i++){
			sumX+= points.get(i).x;
			sumY+= points.get(i).y;
		}
		
		Point avg = new Point(sumX/size, sumY/size);
		
		return avg;
	}
}
