package StandardKMeans;
import java.io.*;
import java.util.*;

public class Main {
	public static void main (String[]args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Please enter name of file: ");
		String fileName = br.readLine();
		BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
		StringTokenizer st;
		
		System.out.println("Please enter number of points: ");
		int numPoints = Integer.parseInt(br.readLine());
		Point[] points = new Point[numPoints]; 
		
		for(int i=0; i<numPoints; i++){
			st = new StringTokenizer(reader.readLine());
			points[i] = new Point(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()));
		}
		
		reader.close();
		
		System.out.println("Please enter number of clusters: ");
		int numClusters = Integer.parseInt(br.readLine());
		
		System.out.println("Choose clustering algorithm:\n1.K-Means ");
		int choice = Integer.parseInt(br.readLine());
		
		System.out.println("Clustering algorithm starting!");
		ArrayList<Point>[] clusters;
		
		switch(choice){
		case 1:
			clusters = KMeans.clusterPoints(points, numClusters); //TODO: Run kmeans 100 times and pick best
			break;
		default:
			clusters = new ArrayList[numClusters];
		}
		
		System.out.println("Clustering algorithm has ended!");
		
		System.out.println("Would you like to see the resulting clusters?\n1.Yes 2.No");
		choice = Integer.parseInt(br.readLine());
		
		if(choice == 1){
			for(int i=0; i<numClusters; i++){
				System.out.println("Cluster #" + (i+1) + " has the following points: ");
				for(int j=0; j<clusters[i].size(); j++) System.out.println(clusters[i].get(j));
			}
		}
	}
}
