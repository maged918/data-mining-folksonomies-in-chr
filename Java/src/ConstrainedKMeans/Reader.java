package ConstrainedKMeans;
import java.util.*;
import java.io.*;

import jpl.Query;
public class Reader {
	
	static int len;
	static int k;
	static int iterations;
	static int dimensions;
	static int users;
	static int links;
	
	static ArrayList<Integer>[] clusters;
	static TreeMap<String, Integer>[] clusterTags;
	static ArrayList<String>[] itemTags;
	static Map<String, Integer> countTags;
	static Map<String, Integer> countItemTags;
	static Map<String, Integer> countUserTags;
	
	static int uniqueTags;
	static int totalTags;
	static int countLinks;
	
	static BufferedReader br;
	static String query;
	static String queryLinks;
	static int choice;
	
	public static void main (String[]args) throws IOException{
				
		File file = new File("clusters.txt");
		file.delete();
		file.createNewFile();
		
		len = 100;
		k = 10;
		iterations = 5;
		dimensions = 20;
		users = 100;
		links = 50;
		
		clusters = new ArrayList[k+1];
		clusterTags = new TreeMap[k+1];
		itemTags = new ArrayList[len+1];
		countTags = new HashMap<String,Integer>();
		countItemTags = new HashMap<String, Integer>();
		countUserTags = new HashMap<String, Integer>();
		
		uniqueTags = 0;
		totalTags = 0;
		countLinks = 0;
		
		br = new BufferedReader(new InputStreamReader(System.in));
		query = "";
		queryLinks = "";
		
		System.out.println("Select data set: ");
		System.out.println("1. Flickr");
		System.out.println("2. Last.fm");
		
		choice = Integer.parseInt(br.readLine());
		
		switch(choice){
		case 1: readFlickr();
				break;
		case 2: readLastfm();
				inferLinks();
				readLinks();
				break;
		}
		
		
		
		query += "start, " + "countClusters(" + k + "), iterate(" + iterations +"),"
				+ " totalLinks(" + countLinks + ").";
		System.out.println(query);
		System.out.printf("Starting K-Means clustering with %d items, k = %d, for %d iterations \n", 
				len, k, iterations);
		System.out.printf("Number of users = %d, number of unique tags = %d, and total number of tags = %d\n", 
				users, uniqueTags, totalTags);
		
		Query q;
		q = new Query("consult('../Prolog/ConstrainedClustering.pl')");
		System.err.println(q.hasSolution());
		
		long start = System.nanoTime();
		q = new Query(query);
		System.err.println(q.hasSolution());
		long end = System.nanoTime();
		System.out.printf("Clustering took %d milliseconds\n", (end-start)/1000000);
		
		readClusters();
		//readClusterTags();
		
		printClusters();
	}
	
	public static void readFlickr() throws IOException{
		
		for(int i=1; i<=len; i++){
			
			itemTags[i] = new ArrayList<String>();
			String fileName = "tags" + i +".txt";
			
			br = new BufferedReader(new FileReader("../Data Sets/mirflickr/meta/tags/" + fileName));
			String line;
			
			while( (line = br.readLine()) != null){
				
				//Keep only a-z, A-Z
				line = line.replaceAll("[\\W\\d]", "");
				if(line.equals("")) continue;
				
				itemTags[i].add(line);
				query+= "ann(u1, i" + i + ", " + line + "),";
				totalTags++;
				if(!countTags.containsKey(line)){
					countTags.put(line,1);
					uniqueTags++;
				}
				else{
					countTags.put(line, countTags.get(line)+1);
				}
			}
		}
		
		users = 1;
		
	}
	
	public static void readLastfm() throws IOException{
		
		br = new BufferedReader(new FileReader("../Data Sets/lastfm/user_taggedartists.dat"));
		br.readLine();
		String line = "";
		
		while( (line = br.readLine()) != null){
			StringTokenizer st = new StringTokenizer(line);
			String user = st.nextToken();
			String item = st.nextToken();
			String tag = st.nextToken();
			
			if(!countItemTags.containsKey(item)) countItemTags.put(item, 1);
			else countItemTags.put(item, countItemTags.get(item)+1);
			
			if(!countTags.containsKey(tag)) countTags.put(tag, 1);
			else countTags.put(tag, countTags.get(tag)+1);
			
			if(!countUserTags.containsKey(user)) countUserTags.put(user, 1);
			else countUserTags.put(user, countUserTags.get(user)+1);
		}
		countTags = MapUtils.sortByValue(countTags);
		countItemTags = MapUtils.sortByValue(countItemTags);
		countUserTags = MapUtils.sortByValue(countUserTags);
		
		
		Set<String> allowedTags = new HashSet<String>();
		Set<String> allowedItems = new HashSet<String>();
		Set<String> allowedUsers = new HashSet<String>();
		
		for(Map.Entry<String, Integer> entry : countTags.entrySet()){
			allowedTags.add(entry.getKey());
			if(allowedTags.size() == dimensions) break;
		}
		
		for(Map.Entry<String, Integer> entry: countItemTags.entrySet()){
			allowedItems.add(entry.getKey());
			if(allowedItems.size() == len) break;
		}
		
		for(Map.Entry<String, Integer> entry: countUserTags.entrySet()){
			allowedUsers.add(entry.getKey());
			if(allowedUsers.size() == users) break;
		}
		
		br = new BufferedReader(new FileReader("../Data Sets/lastfm/user_taggedartists.dat"));
		br.readLine();
		
		while( (line = br.readLine()) != null){
			StringTokenizer st = new StringTokenizer(line);
			String user = st.nextToken();
			String item = st.nextToken();
			String tag = st.nextToken();
			
			if(allowedItems.contains(item) && allowedTags.contains(tag) && allowedUsers.contains(user)){
				query+= "ann(" + user + ", " + item + ", " + tag + "),";
				queryLinks+="userTaggedItem(" + user + ", " + item + "),";
				totalTags++;
			}
			
		}
		queryLinks = queryLinks.substring(0, queryLinks.length()-1) + ".";
		uniqueTags = dimensions;
		
	}
	
	public static void inferLinks() throws IOException{
		File file = new File("links.txt");
		file.delete();
		
		Query q = new Query("consult('../Prolog/LinkInference.pl')");
		System.err.println(q.hasSolution());
		q = new Query(queryLinks);
		
		System.err.println(q.hasSolution());
	}
	
	public static void readLinks() throws IOException{
		
		br = new BufferedReader(new FileReader("links.txt"));
		String line = "";
		Map<Pair<Integer>, Integer> pairs = new HashMap<Pair<Integer>, Integer>();
		
		while((line=br.readLine())!=null){
			StringTokenizer st = new StringTokenizer(line, ":");
			int first = Integer.parseInt(st.nextToken());
			int second = Integer.parseInt(st.nextToken());
			Pair p = new Pair<Integer>(first, second);
			if(!pairs.containsKey(p)) pairs.put(p, 1);
			else pairs. put(p, pairs.get(p) + 1);
		}
		
		pairs = MapUtils.sortByValue(pairs);
		
		for(Map.Entry<Pair<Integer>, Integer> entry : pairs.entrySet()){
			if(countLinks == links) break;
			Pair<Integer> p = entry.getKey();
			query+= "mustLink(" + p.getFirst() + ", " + p.getSecond() + ", " + entry.getValue() + "),";
			countLinks++;
		}
		
		System.out.println(query);
	}
	
	public static void readClusters() throws IOException{
		
		br = new BufferedReader(new FileReader("clusters.txt"));
		int countItems = 0;
		
		for(int i=1; i<=k; i++){
			StringTokenizer st = new StringTokenizer(br.readLine(), ":");
			int id = Integer.parseInt(st.nextToken());
			clusters[id] = new ArrayList<Integer>();
			clusterTags[id] = new TreeMap<String, Integer>();
			
			st = new StringTokenizer(st.nextToken(), "[], ");
			
			while(st.hasMoreTokens()){
				countItems++;
				switch(choice){
				case 1:
					clusters[id].add(Integer.parseInt(st.nextToken().substring(1)));
					break;
				case 2:
					clusters[id].add(Integer.parseInt(st.nextToken()));
					break;
				}
			}
			
			Object[] arr = clusters[id].toArray();
			Arrays.sort(arr);
			clusters[id] = new ArrayList<Integer>();
			for(int j=0; j<arr.length; j++) clusters[id].add((Integer)arr[j]);
			
		}
		System.out.printf("Found %d items in clusters.txt\n", countItems);
	}
	
	public static void readClusterTags(){
		
		for(int id =1; id<=k ; id++){
			for(int j=0; j<clusters[id].size(); j++){
				int item = clusters[id].get(j);
				for(int m=0; m<itemTags[item].size(); m++){
					String tag = itemTags[item].get(m);
					if(clusterTags[id].containsKey(tag)) clusterTags[id].put(tag, clusterTags[id].get(tag)+1);
					else clusterTags[id].put(tag, 1);
				}
			}
		}
	}
	
	public static void printClusters(){
		
		for(int i=1; i<=k; i++){
			System.out.print("Cluster #" + i + ": ");
			for(int j=0; j<clusters[i].size(); j++){
				if(j!=0) System.out.print(", ");
				System.out.print(clusters[i].get(j));
			}
			System.out.println();
			//System.out.println(clusterTags[i]);
		}
	}
	
}
