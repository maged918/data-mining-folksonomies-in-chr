-- MySQL dump 10.13  Distrib 5.6.19, for Linux (x86_64)
--
-- Host: localhost    Database: TagIt
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annotations`
--

DROP TABLE IF EXISTS `annotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotations` (
  `user_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`tag_id`,`photo_id`),
  KEY `annotations_photo_id_fk` (`photo_id`),
  KEY `annotations_tag_id_fk` (`tag_id`),
  CONSTRAINT `annotations_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `annotations_tag_id_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`),
  CONSTRAINT `annotations_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotations`
--

LOCK TABLES `annotations` WRITE;
/*!40000 ALTER TABLE `annotations` DISABLE KEYS */;
INSERT INTO `annotations` VALUES (1,66,63,'2014-07-27 16:56:25'),(1,1,64,'2014-07-27 16:56:09'),(1,3,64,'2014-07-27 16:56:11'),(1,5,64,'2014-07-27 16:56:11'),(1,6,64,'2014-07-27 16:56:12'),(1,7,64,'2014-07-27 16:56:12'),(1,10,64,'2014-07-27 16:56:12'),(1,15,66,'2014-07-27 16:56:13'),(1,1,68,'2014-07-27 16:56:09'),(1,2,68,'2014-07-27 16:56:10'),(1,4,68,'2014-07-27 16:56:11'),(1,22,68,'2014-07-27 16:56:14'),(1,25,68,'2014-07-27 16:56:15'),(1,71,68,'2014-07-27 16:56:26'),(1,73,68,'2014-07-27 16:56:26'),(1,74,68,'2014-07-27 16:56:26'),(1,5,69,'2014-07-27 16:56:11'),(1,73,69,'2014-07-27 16:56:26'),(1,6,70,'2014-07-27 16:56:11'),(1,8,70,'2014-07-27 16:56:12'),(1,9,70,'2014-07-27 16:56:12'),(1,11,71,'2014-07-27 16:56:12'),(1,14,71,'2014-07-27 16:56:13'),(1,12,72,'2014-07-27 16:56:12'),(1,53,72,'2014-07-27 16:56:21'),(1,57,72,'2014-07-27 16:56:22'),(1,59,72,'2014-07-27 16:56:23'),(1,13,73,'2014-07-27 16:56:12'),(1,16,74,'2014-07-27 16:56:14'),(1,19,74,'2014-07-27 16:56:14'),(1,21,74,'2014-07-27 16:56:14'),(1,24,74,'2014-07-27 16:56:14'),(1,25,74,'2014-07-27 16:56:14'),(1,17,75,'2014-07-27 16:56:14'),(1,18,76,'2014-07-27 16:56:14'),(1,20,77,'2014-07-27 16:56:14'),(1,23,78,'2014-07-27 16:56:14'),(1,24,79,'2014-07-27 16:56:14'),(1,52,79,'2014-07-27 16:56:21'),(1,71,79,'2014-07-27 16:56:25'),(1,26,80,'2014-07-27 16:56:15'),(1,27,81,'2014-07-27 16:56:15'),(1,30,81,'2014-07-27 16:56:16'),(1,28,82,'2014-07-27 16:56:15'),(1,29,82,'2014-07-27 16:56:16'),(1,32,82,'2014-07-27 16:56:16'),(1,33,82,'2014-07-27 16:56:17'),(1,29,83,'2014-07-27 16:56:15'),(1,49,83,'2014-07-27 16:56:20'),(1,52,83,'2014-07-27 16:56:21'),(1,31,84,'2014-07-27 16:56:16'),(1,35,84,'2014-07-27 16:56:17'),(1,32,85,'2014-07-27 16:56:16'),(1,34,85,'2014-07-27 16:56:17'),(1,39,85,'2014-07-27 16:56:18'),(1,41,85,'2014-07-27 16:56:19'),(1,33,86,'2014-07-27 16:56:16'),(1,50,86,'2014-07-27 16:56:20'),(1,35,87,'2014-07-27 16:56:17'),(1,36,88,'2014-07-27 16:56:17'),(1,37,89,'2014-07-27 16:56:18'),(1,9,90,'2014-07-27 16:56:12'),(1,38,90,'2014-07-27 16:56:18'),(1,40,91,'2014-07-27 16:56:19'),(1,42,92,'2014-07-27 16:56:19'),(1,43,93,'2014-07-27 16:56:19'),(1,44,93,'2014-07-27 16:56:19'),(1,46,93,'2014-07-27 16:56:20'),(1,45,94,'2014-07-27 16:56:20'),(1,47,95,'2014-07-27 16:56:20'),(1,48,96,'2014-07-27 16:56:20'),(1,51,96,'2014-07-27 16:56:20'),(1,54,97,'2014-07-27 16:56:21'),(1,67,97,'2014-07-27 16:56:25'),(1,55,98,'2014-07-27 16:56:21'),(1,56,99,'2014-07-27 16:56:22'),(1,57,100,'2014-07-27 16:56:22'),(1,58,101,'2014-07-27 16:56:23'),(1,61,101,'2014-07-27 16:56:24'),(1,62,101,'2014-07-27 16:56:24'),(1,60,102,'2014-07-27 16:56:23'),(1,61,103,'2014-07-27 16:56:23'),(1,62,104,'2014-07-27 16:56:24'),(1,63,105,'2014-07-27 16:56:24'),(1,64,106,'2014-07-27 16:56:24'),(1,65,107,'2014-07-27 16:56:24'),(1,68,108,'2014-07-27 16:56:25'),(1,69,109,'2014-07-27 16:56:25'),(1,70,110,'2014-07-27 16:56:25'),(1,72,111,'2014-07-27 16:56:26'),(1,75,112,'2014-07-27 16:56:26'),(1,77,112,'2014-07-27 16:56:26'),(1,78,112,'2014-07-27 16:56:27'),(1,76,113,'2014-07-27 16:56:26'),(1,2,114,'2014-07-27 16:56:11'),(1,13,115,'2014-07-27 16:56:13'),(1,15,115,'2014-07-27 16:56:14'),(1,14,116,'2014-07-27 16:56:13'),(1,27,117,'2014-07-27 16:56:15'),(1,35,118,'2014-07-27 16:56:17'),(1,38,119,'2014-07-27 16:56:18'),(1,44,120,'2014-07-27 16:56:19'),(1,50,121,'2014-07-27 16:56:20'),(1,55,122,'2014-07-27 16:56:22'),(1,60,122,'2014-07-27 16:56:23'),(1,67,122,'2014-07-27 16:56:25'),(1,69,122,'2014-07-27 16:56:25'),(1,65,123,'2014-07-27 16:56:25'),(1,37,124,'2014-07-27 16:58:37'),(1,118,125,'2014-07-27 18:13:43'),(1,132,126,'2014-07-27 18:33:32'),(6,19,76,'2014-07-27 22:07:09'),(6,46,83,'2014-07-27 22:10:22'),(6,36,85,'2014-07-27 21:41:37'),(6,12,105,'2014-07-27 22:10:02');
/*!40000 ALTER TABLE `annotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clusterings`
--

DROP TABLE IF EXISTS `clusterings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusterings` (
  `clustering_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mode_id` int(11) NOT NULL,
  `k` int(11) NOT NULL,
  `constrained` int(11) NOT NULL,
  `limit_items` int(11) NOT NULL,
  `rating` double NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`clustering_id`),
  KEY `clustering_user_id_fk` (`user_id`),
  CONSTRAINT `clustering_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clusterings`
--

LOCK TABLES `clusterings` WRITE;
/*!40000 ALTER TABLE `clusterings` DISABLE KEYS */;
INSERT INTO `clusterings` VALUES (1,1,0,15,0,0,5,'2014-07-27 19:15:51'),(2,1,0,15,1,0,4,'2014-07-27 19:17:05');
/*!40000 ALTER TABLE `clusterings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clusters`
--

DROP TABLE IF EXISTS `clusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusters` (
  `clustering_id` int(11) NOT NULL,
  `cluster_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  PRIMARY KEY (`clustering_id`,`cluster_id`,`photo_id`),
  KEY `clusters_photo_id_fk` (`photo_id`),
  CONSTRAINT `clusters_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clusterings` (`clustering_id`),
  CONSTRAINT `clusters_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clusters`
--

LOCK TABLES `clusters` WRITE;
/*!40000 ALTER TABLE `clusters` DISABLE KEYS */;
INSERT INTO `clusters` VALUES (1,6,1),(2,11,1),(1,0,2),(2,2,2),(1,6,3),(2,11,3),(1,0,4),(2,2,4),(1,8,5),(2,11,5),(1,6,6),(2,11,6),(1,6,7),(2,11,7),(1,0,8),(2,0,8),(1,0,9),(2,0,9),(1,6,10),(2,11,10),(1,0,11),(2,0,11),(1,1,12),(2,1,12),(1,0,13),(2,14,13),(1,0,14),(2,0,14),(1,0,15),(2,14,15),(1,0,16),(2,13,16),(1,0,17),(2,0,17),(1,0,18),(2,0,18),(1,0,19),(2,13,19),(1,0,20),(2,0,20),(1,0,21),(2,13,21),(1,0,22),(2,2,22),(1,0,23),(2,0,23),(1,11,24),(2,13,24),(1,0,25),(2,8,25),(1,0,26),(2,0,26),(1,4,27),(2,0,27),(1,5,28),(2,0,28),(1,5,29),(2,0,29),(1,4,30),(2,0,30),(1,9,31),(2,0,31),(1,5,32),(2,0,32),(1,5,33),(2,0,33),(1,5,34),(2,0,34),(1,9,35),(2,0,35),(1,0,36),(2,0,36),(1,0,37),(2,0,37),(1,0,38),(2,0,38),(1,5,39),(2,0,39),(1,0,40),(2,0,40),(1,5,41),(2,0,41),(1,0,42),(2,5,42),(1,0,43),(2,0,43),(1,0,44),(2,0,44),(1,14,45),(2,0,45),(1,0,46),(2,0,46),(1,0,47),(2,0,47),(1,1,48),(2,1,48),(1,11,49),(2,1,49),(1,0,50),(2,1,50),(1,1,51),(2,1,51),(1,11,52),(2,1,52),(1,1,53),(2,1,53),(1,1,54),(2,4,54),(1,1,55),(2,4,55),(1,1,56),(2,1,56),(1,1,57),(2,1,57),(1,1,58),(2,1,58),(1,1,59),(2,1,59),(1,1,60),(2,4,60),(1,1,61),(2,1,61),(1,1,62),(2,1,62),(1,1,63),(2,12,63),(1,1,64),(2,1,64),(1,1,65),(2,1,65),(1,1,66),(2,1,66),(1,1,67),(2,4,67),(1,1,68),(2,1,68),(1,1,69),(2,4,69),(1,1,70),(2,1,70),(1,11,71),(2,2,71),(1,1,72),(2,1,72),(1,8,73),(2,2,73),(1,0,74),(2,2,74),(1,1,75),(2,1,75),(1,1,76),(2,1,76),(1,1,77),(2,1,77),(1,1,78),(2,1,78),(1,2,79),(2,3,79),(1,2,80),(2,3,80),(1,2,81),(2,3,81),(1,2,82),(2,3,82),(1,2,83),(2,3,83),(1,2,84),(2,3,84),(1,2,85),(2,3,85),(1,2,86),(2,3,86),(1,2,87),(2,3,87),(1,2,88),(2,3,88),(1,2,89),(2,3,89),(1,2,90),(2,3,90),(1,2,91),(2,3,91),(1,2,92),(2,3,92),(1,2,93),(2,3,93),(1,2,94),(2,3,94),(1,2,95),(2,3,95),(1,2,96),(2,3,96),(1,2,97),(2,3,97),(1,2,98),(2,3,98),(1,2,99),(2,3,99),(1,2,100),(2,3,100),(1,2,101),(2,3,101),(1,2,102),(2,3,102),(1,2,103),(2,3,103),(1,2,104),(2,3,104),(1,2,105),(2,3,105),(1,2,106),(2,3,106),(1,2,107),(2,3,107),(1,2,108),(2,3,108),(1,3,109),(2,6,109),(1,3,110),(2,6,110),(1,3,111),(2,6,111),(1,3,112),(2,6,112),(1,3,113),(2,6,113),(1,3,114),(2,6,114),(1,3,115),(2,6,115),(1,3,116),(2,6,116),(1,3,117),(2,6,117),(1,1,118),(2,1,118),(1,3,119),(2,6,119),(1,3,120),(2,6,120),(1,3,121),(2,6,121),(1,3,122),(2,6,122),(1,3,123),(2,6,123),(1,3,124),(2,6,124),(1,3,125),(2,6,125),(1,3,126),(2,6,126),(1,3,127),(2,6,127),(1,3,128),(2,6,128),(1,3,129),(2,6,129),(1,3,130),(2,6,130),(1,3,131),(2,6,131),(1,1,132),(2,1,132),(1,3,133),(2,6,133),(1,3,134),(2,6,134),(1,3,135),(2,6,135),(1,3,136),(2,6,136),(1,3,137),(2,6,137),(1,3,138),(2,6,138),(1,3,139),(2,6,139),(1,3,140),(2,6,140),(1,7,141),(2,7,141),(1,7,142),(2,7,142),(1,7,143),(2,7,143),(1,7,144),(2,7,144),(1,7,145),(2,7,145),(1,7,146),(2,7,146),(1,7,147),(2,7,147),(1,7,148),(2,7,148),(1,7,149),(2,7,149),(1,7,150),(2,7,150),(1,7,151),(2,7,151),(1,7,152),(2,7,152),(1,7,153),(2,7,153),(1,7,154),(2,7,154);
/*!40000 ALTER TABLE `clusters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`photo_id`),
  KEY `photos_user_id_fk` (`user_id`),
  CONSTRAINT `photos_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,1,'images/Photos/','Photo','2014-07-27 15:38:24'),(2,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(3,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(4,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(5,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(6,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(7,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(8,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(9,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(10,1,'images/Photos/','Photo','2014-07-27 15:38:25'),(11,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(12,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(13,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(14,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(15,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(16,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(17,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(18,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(19,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(20,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(21,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(22,1,'images/Photos/','Photo','2014-07-27 15:38:26'),(23,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(24,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(25,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(26,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(27,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(28,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(29,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(30,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(31,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(32,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(33,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(34,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(35,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(36,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(37,1,'images/Photos/','Photo','2014-07-27 15:38:27'),(38,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(39,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(40,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(41,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(42,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(43,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(44,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(45,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(46,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(47,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(48,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(49,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(50,1,'images/Photos/','Photo','2014-07-27 15:38:28'),(51,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(52,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(53,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(54,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(55,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(56,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(57,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(58,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(59,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(60,1,'images/Photos/','Photo','2014-07-27 15:38:29'),(61,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(62,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(63,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(64,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(65,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(66,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(67,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(68,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(69,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(70,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(71,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(72,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(73,1,'images/Photos/','Photo','2014-07-27 15:38:30'),(74,1,'images/Photos/','Photo','2014-07-27 15:38:31'),(75,1,'images/Photos/','Photo','2014-07-27 15:38:31'),(76,1,'images/Photos/','Photo','2014-07-27 15:38:31'),(77,1,'images/Photos/','Photo','2014-07-27 15:38:31'),(78,1,'images/Photos/','Photo','2014-07-27 15:38:31'),(79,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(80,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(81,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(82,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(83,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(84,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(85,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(86,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(87,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(88,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(89,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(90,1,'images/Photos/','Photo','2014-07-27 15:38:32'),(91,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(92,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(93,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(94,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(95,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(96,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(97,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(98,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(99,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(100,1,'images/Photos/','Photo','2014-07-27 15:38:33'),(101,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(102,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(103,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(104,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(105,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(106,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(107,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(108,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(109,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(110,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(111,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(112,1,'images/Photos/','Photo','2014-07-27 15:38:34'),(113,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(114,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(115,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(116,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(117,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(118,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(119,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(120,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(121,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(122,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(123,1,'images/Photos/','Photo','2014-07-27 15:38:35'),(124,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(125,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(126,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(127,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(128,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(129,1,'images/Photos/','Photo','2014-07-27 15:38:36'),(130,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(131,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(132,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(133,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(134,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(135,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(136,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(137,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(138,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(139,1,'images/Photos/','Photo','2014-07-27 15:38:37'),(140,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(141,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(142,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(143,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(144,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(145,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(146,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(147,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(148,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(149,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(150,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(151,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(152,1,'images/Photos/','Photo','2014-07-27 15:38:38'),(153,1,'images/Photos/','Photo','2014-07-27 15:38:39'),(154,1,'images/Photos/','Photo','2014-07-27 15:38:39');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similarities`
--

DROP TABLE IF EXISTS `similarities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similarities` (
  `user_id` int(11) NOT NULL,
  `photo_1` int(11) NOT NULL,
  `photo_2` int(11) NOT NULL,
  `rating` double NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`photo_1`,`photo_2`),
  KEY `similarities_photo_1_fk` (`photo_1`),
  KEY `similarities_photo_2_fk` (`photo_2`),
  CONSTRAINT `similarities_photo_1_fk` FOREIGN KEY (`photo_1`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `similarities_photo_2_fk` FOREIGN KEY (`photo_2`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `similarities_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similarities`
--

LOCK TABLES `similarities` WRITE;
/*!40000 ALTER TABLE `similarities` DISABLE KEYS */;
INSERT INTO `similarities` VALUES (6,1,2,5,'2014-07-27 21:33:12'),(6,1,3,5,'2014-07-27 21:33:18'),(6,1,4,5,'2014-07-27 21:33:45'),(6,1,5,5,'2014-07-27 21:33:55'),(6,2,1,5,'2014-07-27 21:33:13'),(6,2,3,5,'2014-07-27 21:33:40'),(6,2,4,5,'2014-07-27 21:33:48'),(6,2,5,5,'2014-07-27 21:33:58'),(6,3,1,5,'2014-07-27 21:33:18'),(6,3,2,5,'2014-07-27 21:33:40'),(6,3,4,5,'2014-07-27 21:33:51'),(6,3,5,5,'2014-07-27 21:34:00'),(6,4,1,5,'2014-07-27 21:33:45'),(6,4,2,5,'2014-07-27 21:33:48'),(6,4,3,5,'2014-07-27 21:33:52'),(6,4,5,5,'2014-07-27 21:37:38'),(6,5,1,5,'2014-07-27 21:33:55'),(6,5,2,5,'2014-07-27 21:33:58'),(6,5,3,5,'2014-07-27 21:34:00'),(6,5,4,5,'2014-07-27 21:37:38'),(6,22,79,1,'2014-07-27 22:05:07'),(6,25,124,1,'2014-07-27 22:09:03'),(6,43,150,1,'2014-07-27 22:09:15'),(6,45,50,5,'2014-07-27 22:05:01'),(6,50,45,5,'2014-07-27 22:05:02'),(6,53,71,1,'2014-07-27 22:04:51'),(6,69,101,1,'2014-07-27 22:04:21'),(6,71,53,1,'2014-07-27 22:04:52'),(6,79,22,1,'2014-07-27 22:05:07'),(6,79,144,1,'2014-07-27 21:37:43'),(6,85,123,1,'2014-07-27 22:10:53'),(6,90,103,4,'2014-07-27 22:10:57'),(6,95,154,1,'2014-07-27 22:09:06'),(6,101,69,1,'2014-07-27 22:04:21'),(6,103,90,4,'2014-07-27 22:10:57'),(6,123,85,1,'2014-07-27 22:10:53'),(6,124,25,1,'2014-07-27 22:09:03'),(6,144,79,1,'2014-07-27 21:37:43'),(6,150,43,1,'2014-07-27 22:09:15'),(6,154,95,1,'2014-07-27 22:09:06'),(7,54,134,1,'2014-07-27 22:46:57'),(7,61,67,4,'2014-07-27 22:46:10'),(7,67,61,4,'2014-07-27 22:46:10'),(7,67,118,1,'2014-07-27 22:47:18'),(7,118,67,1,'2014-07-27 22:47:18'),(7,134,54,1,'2014-07-27 22:46:57');
/*!40000 ALTER TABLE `similarities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (63,'pingpong','2014-07-27 16:34:37'),(64,'Jaguar','2014-07-27 16:38:34'),(65,'Jaguars','2014-07-27 16:42:50'),(66,'ping pong','2014-07-27 16:46:04'),(67,'\"ping pong\"','2014-07-27 16:47:00'),(68,'animal','2014-07-27 16:52:37'),(69,'forest','2014-07-27 16:52:38'),(70,'car','2014-07-27 16:52:38'),(71,'bat','2014-07-27 16:52:39'),(72,'sports','2014-07-27 16:52:39'),(73,'tennis','2014-07-27 16:52:39'),(74,'mouse','2014-07-27 16:52:40'),(75,'computer mouse','2014-07-27 16:52:40'),(76,'computer','2014-07-27 16:52:40'),(77,'wireless mouse','2014-07-27 16:52:41'),(78,'mice','2014-07-27 16:52:41'),(79,'cartoon','2014-07-27 16:52:41'),(80,'sliced orange','2014-07-27 16:52:43'),(81,'oranges','2014-07-27 16:52:43'),(82,'orange','2014-07-27 16:52:43'),(83,'fruit','2014-07-27 16:52:43'),(84,'apples','2014-07-27 16:52:44'),(85,'apple','2014-07-27 16:52:44'),(86,'tree','2014-07-27 16:52:44'),(87,'green','2014-07-27 16:52:45'),(88,'green apple','2014-07-27 16:52:46'),(89,'red apple','2014-07-27 16:52:46'),(90,'logo','2014-07-27 16:52:47'),(91,'ipad','2014-07-27 16:52:47'),(92,'iphone','2014-07-27 16:52:47'),(93,'mango','2014-07-27 16:52:47'),(94,'mangos','2014-07-27 16:52:47'),(95,'sliced mangos','2014-07-27 16:52:48'),(96,'banana','2014-07-27 16:52:48'),(97,'ball','2014-07-27 16:52:49'),(98,'goal','2014-07-27 16:52:49'),(99,'football','2014-07-27 16:52:49'),(100,'children','2014-07-27 16:52:49'),(101,'basketball','2014-07-27 16:52:49'),(102,'score','2014-07-27 16:52:50'),(103,'match','2014-07-27 16:52:50'),(104,'court','2014-07-27 16:52:50'),(105,'baseball','2014-07-27 16:52:51'),(106,'baseball bat','2014-07-27 16:52:51'),(107,'table','2014-07-27 16:52:51'),(108,'pingpong bat','2014-07-27 16:52:52'),(109,'player','2014-07-27 16:52:52'),(110,'lion king','2014-07-27 16:52:53'),(111,'lion','2014-07-27 16:52:53'),(112,'screen','2014-07-27 16:52:54'),(113,'computers','2014-07-27 16:52:54'),(114,' baby','2014-07-27 16:56:11'),(115,' bat','2014-07-27 16:56:13'),(116,' sports','2014-07-27 16:56:13'),(117,'cat','2014-07-27 16:56:15'),(118,'red','2014-07-27 16:56:17'),(119,' apple','2014-07-27 16:56:18'),(120,'fruits','2014-07-27 16:56:19'),(121,'bananas','2014-07-27 16:56:20'),(122,'sport','2014-07-27 16:56:22'),(123,' sport','2014-07-27 16:56:25'),(124,'nice apple','2014-07-27 16:58:36'),(125,'drums','2014-07-27 18:13:43'),(126,'nurse','2014-07-27 18:33:32');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(512) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Maged','Shalaby','maged918@gmail.com','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-16 16:34:57','admin'),(2,'Maged','Shalaby','maged.shalaby@student.guc.edu.eg','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-22 15:44:03','maged'),(4,'Maged','Shalaby','maged.shalaby@guc.insiderstudent.com','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-22 18:24:58','maged918'),(5,'Randa','Ali','randaali311@hotmail.com','89cb84edf017b50cc8c577809930a0f7a6382ab6dcfbbfd5feaf0329aa7b0e699fa99c5d63693e6ec9d3f9f6cf8ae5a4c6204311b289430a0755210e4d791cb5','2014-07-22 18:31:40','randaali'),(6,'Test','Test','test@test.com','3dae98c3358552328a52eacbbcf05f22bf25abf52e75f1ea965e23584a818ab4dc0d50bbf611b78a51ac349a0614a677724997138140bf0df62521c4dd584b64','2014-07-26 22:13:15','testt'),(7,'Mostafa','El Beheiry','mostafa.beheiry.studios@gmail.com','b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86','2014-07-27 19:52:29','caesariv'),(8,'Islam','El Lawendi ','i.ellawendi@gmail.com','dbf0d7abe370d597351ec052effddf18b675c75f72c342c6a6eab6a16997eec495847f4f3bb80db629da2b0ea5613b2b3c73827d585cd14e0918f275bae01dc9','2014-07-27 21:16:52','essoz'),(9,'ahmed','elsayed','ahmedelsayed927@hotmail.com','ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413','2014-07-27 21:16:59','ahmedelsayed'),(10,'Ali','Said','alimoussasaid@gmail.com','915074bb48b46a56bc4ce63868983203247c342da0638c440dcedc86cf99e8b3b48ca4d3beec7b0c41e65ab79c48e62ce81690dc6c4fa01ff09380148c4828fc','2014-07-27 22:25:15','alito');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-28  2:47:58
