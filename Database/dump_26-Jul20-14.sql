-- MySQL dump 10.13  Distrib 5.6.19, for Linux (x86_64)
--
-- Host: localhost    Database: TagIt
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annotations`
--

DROP TABLE IF EXISTS `annotations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `annotations` (
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`tag_id`,`photo_id`),
  KEY `annotations_tag_id_fk` (`tag_id`),
  KEY `annotations_photo_id_fk` (`photo_id`),
  CONSTRAINT `annotations_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `annotations_tag_id_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`),
  CONSTRAINT `annotations_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `annotations`
--

LOCK TABLES `annotations` WRITE;
/*!40000 ALTER TABLE `annotations` DISABLE KEYS */;
INSERT INTO `annotations` VALUES (1,1,1,'2014-07-22 19:27:28'),(1,1,2,'2014-07-22 20:04:43'),(1,1,3,'2014-07-23 03:02:53'),(1,5,2,'2014-07-22 20:04:39'),(1,10,2,'2014-07-22 20:03:13'),(1,14,1,'2014-07-22 20:03:34'),(1,15,2,'2014-07-22 20:04:41'),(1,17,2,'2014-07-23 02:44:47'),(1,17,3,'2014-07-23 03:02:22'),(2,14,1,'2014-07-23 00:05:21'),(5,10,2,'2014-07-25 20:24:35'),(5,16,3,'2014-07-23 03:07:58'),(5,17,1,'2014-07-23 03:06:55'),(5,17,3,'2014-07-25 20:23:10'),(5,19,2,'2014-07-25 19:45:36'),(5,19,3,'2014-07-23 03:07:42'),(5,20,2,'2014-07-23 03:11:26'),(5,22,3,'2014-07-25 18:42:52'),(5,23,3,'2014-07-25 20:19:22');
/*!40000 ALTER TABLE `annotations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_modes`
--

DROP TABLE IF EXISTS `clustering_modes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_modes` (
  `mode_id` int(11) NOT NULL AUTO_INCREMENT,
  `mode_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`mode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_modes`
--

LOCK TABLES `clustering_modes` WRITE;
/*!40000 ALTER TABLE `clustering_modes` DISABLE KEYS */;
INSERT INTO `clustering_modes` VALUES (1,'Uploaded Photos'),(2,'Tagged Photos'),(3,'Friends\' Photos'),(4,'All Photos');
/*!40000 ALTER TABLE `clustering_modes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clusterings`
--

DROP TABLE IF EXISTS `clusterings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusterings` (
  `clustering_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `mode_id` int(11) NOT NULL,
  `k` int(11) NOT NULL,
  `constrained` int(11) NOT NULL,
  `limit_items` int(11) NOT NULL,
  `rating` double NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`clustering_id`),
  KEY `clustering_user_id_fk` (`user_id`),
  CONSTRAINT `clustering_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clusterings`
--

LOCK TABLES `clusterings` WRITE;
/*!40000 ALTER TABLE `clusterings` DISABLE KEYS */;
INSERT INTO `clusterings` VALUES (1,1,0,2,0,0,4,'2014-07-26 19:16:44'),(2,1,0,2,0,0,4,'2014-07-26 19:16:51'),(3,1,0,2,0,0,4,'2014-07-26 19:17:12'),(4,1,0,2,0,0,4,'2014-07-26 19:25:04'),(5,1,0,2,0,0,4,'2014-07-26 19:25:26'),(6,1,0,2,0,0,5,'2014-07-26 19:25:44'),(7,1,0,2,0,0,5,'2014-07-26 19:26:32'),(8,1,0,2,0,0,5,'2014-07-26 19:26:43'),(9,1,0,15,0,0,4.5,'2014-07-26 19:33:51'),(10,1,0,15,0,0,4.5,'2014-07-26 19:34:47'),(11,1,0,15,0,0,4.5,'2014-07-26 20:17:18'),(12,1,0,15,0,0,4,'2014-07-26 20:17:50'),(13,1,0,15,0,0,3.5,'2014-07-26 20:19:48'),(14,1,0,15,0,0,4,'2014-07-26 20:22:32'),(15,1,0,2,0,0,4,'2014-07-26 20:24:03'),(16,1,0,2,0,0,3.5,'2014-07-26 20:25:21'),(17,1,0,2,0,0,4,'2014-07-26 20:27:10'),(18,1,0,2,0,0,4,'2014-07-26 20:29:02'),(19,1,0,2,0,0,4,'2014-07-26 20:29:23'),(20,1,0,3,0,0,4,'2014-07-26 20:37:18'),(21,1,0,3,0,0,3,'2014-07-26 20:42:43'),(22,1,0,3,1,0,3,'2014-07-26 20:42:56'),(23,1,0,3,0,0,5,'2014-07-26 20:46:25'),(24,1,0,3,1,0,3.5,'2014-07-26 20:46:42'),(25,1,0,3,0,0,5,'2014-07-26 20:47:30'),(26,1,0,3,1,0,5,'2014-07-26 20:47:41'),(27,1,0,3,0,0,5,'2014-07-26 20:48:33'),(28,1,0,3,1,0,5,'2014-07-26 20:48:49'),(29,1,0,3,0,0,5,'2014-07-26 21:29:47'),(30,1,0,3,1,0,5,'2014-07-26 21:30:17'),(31,1,0,3,0,0,4.5,'2014-07-26 21:34:00'),(32,1,0,3,1,0,4.5,'2014-07-26 21:34:11'),(33,1,0,3,0,0,5,'2014-07-26 22:02:21'),(34,1,0,3,1,0,5,'2014-07-26 22:02:27'),(35,1,0,2,0,0,5,'2014-07-26 22:02:49'),(36,1,0,2,1,0,5,'2014-07-26 22:02:58'),(37,5,0,3,0,0,5,'2014-07-26 22:10:43'),(38,5,0,3,1,0,5,'2014-07-26 22:11:07');
/*!40000 ALTER TABLE `clusterings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clusters`
--

DROP TABLE IF EXISTS `clusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusters` (
  `clustering_id` int(11) NOT NULL,
  `cluster_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  PRIMARY KEY (`clustering_id`,`cluster_id`,`photo_id`),
  KEY `clusters_photo_id_fk` (`photo_id`),
  CONSTRAINT `clusters_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clusterings` (`clustering_id`),
  CONSTRAINT `clusters_photo_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clusters`
--

LOCK TABLES `clusters` WRITE;
/*!40000 ALTER TABLE `clusters` DISABLE KEYS */;
INSERT INTO `clusters` VALUES (11,2,1),(12,0,1),(13,1,1),(14,4,1),(15,1,1),(16,1,1),(17,0,1),(18,0,1),(19,0,1),(20,0,1),(21,2,1),(22,1,1),(23,0,1),(24,0,1),(25,0,1),(26,0,1),(27,1,1),(28,2,1),(29,1,1),(30,2,1),(31,1,1),(32,0,1),(33,2,1),(34,2,1),(35,0,1),(36,1,1),(37,0,1),(38,1,1),(1,1,2),(13,1,2),(14,4,2),(15,1,2),(16,1,2),(17,0,2),(18,0,2),(19,0,2),(20,0,2),(21,2,2),(22,1,2),(23,0,2),(24,0,2),(25,0,2),(26,0,2),(27,1,2),(28,2,2),(29,1,2),(30,2,2),(31,1,2),(32,0,2),(33,2,2),(34,2,2),(35,0,2),(36,1,2),(37,0,2),(38,1,2),(1,1,3),(11,3,3),(12,2,3),(14,4,3),(15,1,3),(16,1,3),(17,0,3),(18,0,3),(19,1,3),(20,0,3),(21,2,3),(22,1,3),(23,1,3),(24,1,3),(25,2,3),(26,2,3),(27,2,3),(28,0,3),(29,2,3),(30,1,3),(31,2,3),(32,2,3),(33,1,3),(34,1,3),(35,1,3),(36,0,3),(37,2,3),(38,2,3),(20,0,4),(21,0,4),(22,0,4),(23,0,4),(24,0,4),(25,0,4),(26,0,4),(27,0,4),(28,0,4),(29,0,4),(30,0,4),(31,0,4),(32,0,4),(33,0,4),(34,0,4),(35,0,4),(36,0,4),(37,0,4),(38,0,4);
/*!40000 ALTER TABLE `clusters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `photos_user_id_fk` (`user_id`),
  CONSTRAINT `photos_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,1,'German Shephard 1','2014-07-20 19:08:10','images/personal/dogs/'),(2,1,'Golden Retriever 1','2014-07-20 19:08:11','images/personal/dogs/'),(3,1,'Husky 1','2014-07-20 19:08:11','images/personal/dogs/'),(4,1,'White Shephard 1','2014-07-26 20:32:51','/images/personal/dogs/');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similarities`
--

DROP TABLE IF EXISTS `similarities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similarities` (
  `user_id` int(11) NOT NULL,
  `photo_1` int(11) NOT NULL,
  `photo_2` int(11) NOT NULL,
  `rating` double NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`photo_1`,`photo_2`),
  KEY `similarities_photo_1_fk` (`photo_1`),
  KEY `similarities_photo_2_fk` (`photo_2`),
  CONSTRAINT `similarities_photo_1_fk` FOREIGN KEY (`photo_1`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `similarities_photo_2_fk` FOREIGN KEY (`photo_2`) REFERENCES `photos` (`photo_id`),
  CONSTRAINT `similarities_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similarities`
--

LOCK TABLES `similarities` WRITE;
/*!40000 ALTER TABLE `similarities` DISABLE KEYS */;
INSERT INTO `similarities` VALUES (1,1,2,5,'2014-07-22 18:14:50'),(1,1,3,2,'2014-07-26 22:01:19'),(1,1,4,5,'2014-07-26 20:33:39'),(1,2,1,5,'2014-07-22 18:14:50'),(1,2,3,1,'2014-07-26 20:44:09'),(1,3,1,2,'2014-07-26 22:01:20'),(1,3,2,1,'2014-07-26 20:44:09'),(1,4,1,5,'2014-07-26 20:33:39'),(4,1,2,3,'2014-07-25 22:58:27'),(4,2,1,4,'2014-07-25 22:58:27');
/*!40000 ALTER TABLE `similarities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Dog','2014-07-13 12:24:00'),(2,'Cat','2014-07-13 12:24:01'),(3,'Fish','2014-07-13 12:24:01'),(4,'Animal','2014-07-13 12:24:01'),(5,'Puppy','2014-07-22 18:10:19'),(10,'Cute','2014-07-22 19:30:04'),(14,'German Shephard','2014-07-22 20:03:34'),(15,'Grass','2014-07-22 20:04:41'),(16,'Husky','2014-07-23 02:36:23'),(17,'Dogs','2014-07-23 02:44:47'),(18,'Puppy','2014-07-23 03:01:43'),(19,'White','2014-07-23 03:07:42'),(20,'Outdoor','2014-07-23 03:11:26'),(21,'','2014-07-23 03:11:37'),(22,'Ice','2014-07-25 18:42:52'),(23,'Menacing','2014-07-25 20:19:22');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(512) NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Maged','Shalaby','maged918@gmail.com','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-16 16:34:57','admin'),(2,'Maged','Shalaby','maged.shalaby@student.guc.edu.eg','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-22 15:44:03','maged'),(4,'Maged','Shalaby','maged.shalaby@guc.insiderstudent.com','c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec','2014-07-22 18:24:58','maged918'),(5,'Randa','Ali','randaali311@hotmail.com','89cb84edf017b50cc8c577809930a0f7a6382ab6dcfbbfd5feaf0329aa7b0e699fa99c5d63693e6ec9d3f9f6cf8ae5a4c6204311b289430a0755210e4d791cb5','2014-07-22 18:31:40','randaali'),(6,'Test','Test','test@test.com','3dae98c3358552328a52eacbbcf05f22bf25abf52e75f1ea965e23584a818ab4dc0d50bbf611b78a51ac349a0614a677724997138140bf0df62521c4dd584b64','2014-07-26 22:13:15','testt');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-27  0:14:57
