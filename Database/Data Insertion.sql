INSERT INTO users(user_name,email, pass, first_name, last_name,added)
	VALUES ('admin','maged918@gmail.com', SHA2('admin', 512), 'Maged', 'Shalaby', now());

INSERT INTO tags (tag, added) VALUES ('Dog', now());
INSERT INTO tags (tag, added) VALUES ('Cat', now());
INSERT INTO tags (tag, added) VALUES ('Fish', now());
INSERT INTO tags (tag, added) VALUES ('Animal', now());
INSERT INTO tags (tag, added) VALUES ('Puppy', now());

CALL Annotate(1, 3,'Dogs', @output);
CALL Annotate(1, 1,'German Shephard');
CALL Annotate(2, 1,'German Shephard');

CALL Annotate(1, 2,'Jaguar', @output);
CALL Annotate(1, 2,'Cute');
CALL Annotate(1, 2,'Puppy');
CALL Annotate(1, 2,'Grass');

SET @dogsURL = '/images/personal/dogs/';

INSERT INTO photos(user_id, url, title, added) VALUES (1, @dogsURL, 'White Shephard 1', now());
INSERT INTO photos(user_id, url, title, added) VALUES (1, @dogsURL, 'German Shephard 1', now());
INSERT INTO photos(user_id, url, title, added) VALUES (1, @dogsURL, 'Golden Retriever 1', now());
INSERT INTO photos(user_id, url, title, added) VALUES (1, @dogsURL, 'Husky 1', now());

CALL SaveSimilarity(1, 1, 2, 5);
CALL SaveSimilarity(1, 1, 4, 5);
CALL SaveSimilarity(1, 2, 3, 1);

CALL Annotate(2, 1, 'Jaguar', @output);