DELIMITER //

USE TagIt; //
/*
	levenshtein & levenshtein_ratio
	by Federico Razzoli
	santec@riseup.net
	
	levenshtein is a fork of an Arjen Lentz function (reported below).
*/

-- version: 1

DROP FUNCTION IF EXISTS `levenshtein`; //
CREATE FUNCTION `levenshtein`(`s1` VARCHAR(255) CHARACTER SET utf8, `s2` VARCHAR(255) CHARACTER SET utf8)
	RETURNS TINYINT UNSIGNED
	NO SQL
	DETERMINISTIC
BEGIN
	DECLARE s1_len, s2_len, i, j, c, c_temp TINYINT UNSIGNED;
	-- max strlen=255 for this function
	DECLARE cv0, cv1 VARBINARY(256);
	
	-- if any param is NULL return NULL
	-- (consistent with builtin functions)
	IF (s1 + s2) IS NULL THEN
		RETURN NULL;
	END IF;
	
	SET s1_len = CHAR_LENGTH(s1),
		s2_len = CHAR_LENGTH(s2),
		cv1 = 0x00,
		j = 1,
		i = 1,
		c = 0;
	
	-- if any string is empty,
	-- distance is the length of the other one
	IF (s1 = s2) THEN
		RETURN 0;
	ELSEIF (s1_len = 0) THEN
		RETURN s2_len;
	ELSEIF (s2_len = 0) THEN
		RETURN s1_len;
	END IF;
	
	WHILE (j <= s2_len) DO
		SET cv1 = CONCAT(cv1, CHAR(j)),
		j = j + 1;
	END WHILE;
	
	WHILE (i <= s1_len) DO
		SET c = i,
			cv0 = CHAR(i),
			j = 1;
		
		WHILE (j <= s2_len) DO
			SET c = c + 1;
			
			SET c_temp = ORD(SUBSTRING(cv1, j, 1)) -- ord of cv1 current char
				+ (NOT (SUBSTRING(s1, i, 1) = SUBSTRING(s2, j, 1))); -- different chars? (NULL-safe)
			IF (c > c_temp) THEN
				SET c = c_temp;
			END IF;
			
			SET c_temp = ORD(SUBSTRING(cv1, j+1, 1)) + 1;
			IF (c > c_temp) THEN
				SET c = c_temp;
			END IF;
			
			SET cv0 = CONCAT(cv0, CHAR(c)),
				j = j + 1;
		END WHILE;
		
		SET cv1 = cv0,
			i = i + 1;
	END WHILE;
	
	RETURN c;
END; //


-- version: 1

DROP FUNCTION IF EXISTS `levenshtein_ratio`; //
CREATE FUNCTION `levenshtein_ratio`(`s1` VARCHAR(255) CHARACTER SET utf8, `s2` VARCHAR(255) CHARACTER SET utf8)
	RETURNS TINYINT UNSIGNED
	DETERMINISTIC
	NO SQL
	COMMENT 'Levenshtein ratio between strings'
BEGIN
	DECLARE s1_len TINYINT UNSIGNED DEFAULT CHAR_LENGTH(s1);
	DECLARE s2_len TINYINT UNSIGNED DEFAULT CHAR_LENGTH(s2);
	RETURN ((levenshtein(s1, s2) / IF(s1_len > s2_len, s1_len, s2_len)) * 100);
END; //



DROP PROCEDURE IF EXISTS MinSimOld// 

CREATE PROCEDURE MinSimOld(IN user_id INT)
SELECT 	*
FROM (SELECT p1.photo_id AS 'photo_id_1', p1.title AS 'title_1', p1.url AS 'url_1', 
		p2.photo_id AS 'photo_id_2', p2.title AS 'title_2', p2.url AS 'url_2' 
		FROM photos p1, photos p2) AS p
		LEFT JOIN similarities s
ON p.photo_id_1 = s.photo_1 AND p.photo_id_2 = s.photo_2 AND s.user_id = user_id
WHERE p.photo_id_1 < p.photo_id_2 AND s.user_id IS NULL
ORDER BY rand() DESC LIMIT 1
//

DROP PROCEDURE IF EXISTS MinSimEditDist//

CREATE PROCEDURE MinSimEditDist(IN user_id INT)
SELECT p1.photo_id AS 'photo_id_1', p1.title AS 'title_1', p1.url AS 'url_1', 
		p2.photo_id AS 'photo_id_2', p2.title AS 'title_2', p2.url AS 'url_2', 
		t1.tag AS 'tag_1', t2.tag AS 'tag_2' 
FROM (annotations a1 JOIN tags t1 ON a1.tag_id = t1.tag_id JOIN photos p1 ON a1.photo_id = p1.photo_id)
JOIN (annotations a2 JOIN tags t2 ON a2.tag_id = t2.tag_id JOIN photos p2 ON a2.photo_id = p2.photo_id)
	ON levenshtein_ratio(t1.tag, t2.tag) <= 25 AND a1.photo_id != a2.photo_id
LEFT JOIN similarities s
	ON p1.photo_id = s.photo_1 AND p2.photo_id = s.photo_2 AND s.user_id = user_id
	WHERE p1.photo_id < p2.photo_id AND s.user_id IS NULL
ORDER BY rand() LIMIT 1
//

DROP PROCEDURE IF EXISTS MinSim//

CREATE PROCEDURE MinSim(IN user_id INT)
SELECT p1.photo_id AS 'photo_id_1', p1.title AS 'title_1', p1.url AS 'url_1', 
		p2.photo_id AS 'photo_id_2', p2.title AS 'title_2', p2.url AS 'url_2', 
		t1.tag AS 'tag_1', t2.tag AS 'tag_2' 
FROM (annotations a1 JOIN tags t1 ON a1.tag_id = t1.tag_id JOIN photos p1 ON a1.photo_id = p1.photo_id)
JOIN (annotations a2 JOIN tags t2 ON a2.tag_id = t2.tag_id JOIN photos p2 ON a2.photo_id = p2.photo_id)
	ON t1.tag_id=t2.tag_id AND a1.photo_id != a2.photo_id
LEFT JOIN similarities s
	ON p1.photo_id = s.photo_1 AND p2.photo_id = s.photo_2 AND s.user_id = user_id
	WHERE p1.photo_id < p2.photo_id AND s.user_id IS NULL
ORDER BY rand() LIMIT 1
//

CALL MinSim(1)//


DROP PROCEDURE SaveSimilarity//

CREATE PROCEDURE SaveSimilarity(IN user_id INT, IN photo_1 INT, IN photo_2 INT, IN rating INT, IN experiment_id INT)
	BEGIN
		INSERT INTO similarities VALUES(user_id, photo_1, photo_2, rating, now(), experiment_id)
		ON DUPLICATE KEY UPDATE rating = rating, added=now();
		INSERT INTO similarities VALUES(user_id, photo_2, photo_1, rating, now(), experiment_id)
		ON DUPLICATE KEY UPDATE rating = rating, added=now();
	END//

DROP PROCEDURE Annotate//

CREATE PROCEDURE Annotate(IN user_id INT, IN photo_id INT, IN tag VARCHAR(100), OUT output INT)
BEGIN	
	DECLARE countTags INT;
	/*DECLARE tag_ID INT;*/
	SET countTags = 0;
	SET output = 0;
	SELECT count(*) INTO countTags FROM tags t WHERE t.tag LIKE tag;
	IF countTags = 0
	THEN
		INSERT INTO tags (tag, added) VALUES (tag, now());
	END IF;
	SELECT t.tag_id INTO @tag_id FROM tags t WHERE t.tag LIKE tag;
	
	/* Ensure unique(user, photo, tag)*/
	SELECT count(*) INTO countTags FROM annotations a 
		WHERE a.tag_id = @tag_id AND a.photo_id = photo_id AND a.user_id = user_id;
	IF countTags = 0
	THEN
		INSERT INTO annotations VALUES (user_id,photo_id, @tag_id, now());
		SET output = 1;
	END IF;
END//

DROP PROCEDURE MaxPhotoTags; //

CREATE PROCEDURE MaxPhotoTags(IN photo_id INT)
	SELECT t.tag, count(t.tag_id) AS 'count'
	FROM tags t LEFT OUTER JOIN annotations a
	ON t.tag_id = a.tag_id
	WHERE a.photo_id = photo_id
	GROUP BY t.tag_id
	ORDER BY count(t.tag_id) DESC
	LIMIT 5
//

CALL MaxPhotoTags(1);//

CREATE PROCEDURE AllPhotoTags(IN photo_id INT)
	SELECT t.tag, t.tag_id, count(t.tag_id) AS 'count'
	FROM tags t LEFT OUTER JOIN annotations a
	ON t.tag_id = a.tag_id
	WHERE a.photo_id = photo_id
	GROUP BY t.tag_id
	ORDER BY count(t.tag_id) DESC
//

CALL AllPhotoTags(1);//

DROP PROCEDURE AllPhotosTags//

CREATE PROCEDURE AllPhotosTags()
	SELECT p.photo_id, t.tag, t.tag_id, count(t.tag_id) AS 'count'
	FROM photos p LEFT JOIN annotations a
	ON p.photo_id = a.photo_id
	LEFT JOIN tags t
	ON t.tag_id = a.tag_id
	GROUP BY p.photo_id, t.tag_id
//

CALL AllPhotosTags();//

DROP PROCEDURE AllConstraints;//

CREATE PROCEDURE AllConstraints()
	SELECT photo_1, photo_2, avg(rating) AS 'rating'
	FROM similarities
	GROUP BY photo_1, photo_2
//

CALL AllConstraints();//

DROP PROCEDURE EvaluateClustering;//

CREATE PROCEDURE EvaluateClustering(user_id INT, mode_id INT, k INT, constrained INT, 
	limit_items INT, rating DOUBLE, experiment_id INT, OUT output INT)
BEGIN
	INSERT INTO clusterings(user_id, mode_id, k, constrained, limit_items, rating, added, experiment_id) 
		VALUES(user_id, mode_id, k, constrained, limit_items, rating, now(), experiment_id);
	SET output = LAST_INSERT_ID();
END
//

/*CALL EvaluateClustering(1,0,15,0,0,5,1,@output);//
SELECT @output;//
*/

DROP PROCEDURE AddPhoto;//

CREATE PROCEDURE AddPhoto(photo_id INT)
	INSERT INTO photos (user_id, url, title, added) VALUES (1, '/images/Photos/', 'Photo', now());
//

CALL AddPhoto(1);//

