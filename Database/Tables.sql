CREATE TABLE users(
user_id INTEGER AUTO_INCREMENT NOT NULL,
	CONSTRAINT users_pk
		PRIMARY KEY (user_id),
user_name VARCHAR(100),
	CONSTRAINT users_user_name_unique
		UNIQUE(user_name),
email VARCHAR(100) NOT NULL,
pass VARCHAR(512) NOT NULL,
first_name VARCHAR(100) NOT NULL,
last_name VARCHAR(100) NOT NULL,
added TIMESTAMP NOT NULL
);

CREATE TABLE tags(
tag_id INTEGER AUTO_INCREMENT NOT NULL,
	CONSTRAINT tags_pk
		PRIMARY KEY (tag_id),
tag VARCHAR(100) NOT NULL,
added TIMESTAMP NOT NULL
);

CREATE TABLE photos(
photo_id INTEGER AUTO_INCREMENT NOT NULL,
	CONSTRAINT photos_pk
		PRIMARY KEY (photo_id),
user_id INTEGER NOT NULL,
	CONSTRAINT photos_user_id_fk
		FOREIGN KEY (user_id) REFERENCES users(user_id),
url VARCHAR(100) NOT NULL,
title VARCHAR(100),
added TIMESTAMP NOT NULL
);

CREATE TABLE annotations(
user_id INTEGER NOT NULL,
	CONSTRAINT annotations_user_id_fk
		FOREIGN KEY (user_id) REFERENCES users(user_id),
photo_id INTEGER NOT NULL,
	CONSTRAINT annotations_photo_id_fk
		FOREIGN KEY (photo_id) REFERENCES photos(photo_id),
tag_id INTEGER NOT NULL,
	CONSTRAINT annotations_tag_id_fk
		FOREIGN KEY (tag_id) REFERENCES tags(tag_id),
added TIMESTAMP NOT NULL,
CONSTRAINT annotations_pk
	PRIMARY KEY (user_id, tag_id, photo_id)
);

CREATE TABLE similarities(
user_id INTEGER NOT NULL,
	CONSTRAINT similarities_user_id_fk
		FOREIGN KEY (user_id) REFERENCES users(user_id),
photo_1 INTEGER NOT NULL,
	CONSTRAINT similarities_photo_1_fk
		FOREIGN KEY (photo_1) REFERENCES photos(photo_id),
photo_2 INTEGER NOT NULL,
	CONSTRAINT similarities_photo_2_fk
		FOREIGN KEY (photo_2) REFERENCES photos(photo_id),
rating DOUBLE NOT NULL,
	CONSTRAINT similarities_rating_ck
		CHECK (rating > 0 AND rating < 6),
added TIMESTAMP NOT NULL,
experiment_id INT NOT NULL,
CONSTRAINT similarities_pk
	PRIMARY KEY (user_id, photo_1, photo_2)
);

/*CREATE TABLE clustering_modes(
mode_id INTEGER AUTO_INCREMENT NOT NULL,
	CONSTRAINT clustering_modes_pk
		PRIMARY KEY (mode_id),
mode_name VARCHAR(100)
);

INSERT INTO clustering_modes(mode_name) VALUES ('Uploaded Photos');
INSERT INTO clustering_modes(mode_name) VALUES ('Tagged Photos');
INSERT INTO clustering_modes(mode_name) VALUES ('Friends\' Photos');
INSERT INTO clustering_modes(mode_name) VALUES ('All Photos');*/

CREATE TABLE clusterings(
clustering_id INTEGER AUTO_INCREMENT NOT NULL,
	CONSTRAINT clustering_pk 
		PRIMARY KEY (clustering_id),
user_id INTEGER NOT NULL,
	CONSTRAINT clustering_user_id_fk 
		FOREIGN KEY (user_id) REFERENCES users(user_id),
mode_id INTEGER NOT NULL,
		-- 0: All Photos
k INTEGER NOT NULL,
constrained INTEGER NOT NULL,
		-- 0: Constrained
		-- 1: Not Constrained
limit_items INTEGER NOT NULL,
rating DOUBLE NOT NULL,
	CONSTRAINT clustering_rating_ck
		CHECK (rating > 0 AND rating <6),
added TIMESTAMP NOT NULL,
experiment_id INT NOT NULL
);

CREATE TABLE clusters(
clustering_id INTEGER NOT NULL,
	CONSTRAINT clusters_clustering_id_fk
		FOREIGN KEY (clustering_id) REFERENCES clusterings(clustering_id),
cluster_id INTEGER NOT NULL,
photo_id INTEGER NOT NULL,
	CONSTRAINT clusters_photo_id_fk
		FOREIGN KEY (photo_id) REFERENCES photos(photo_id),
	CONSTRAINT clusters_pk
		PRIMARY KEY (clustering_id, cluster_id, photo_id)
);
